package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class TestWorld {
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	private Random random;
	
	// X X X X X
	// . . . . .
	// . . . . .
	// X X . X X
	// X X . X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false, false, false }, 
			{ true, true, true, true, true, true }, 
			{ true, true, true, true, true, true }, 
			{ true, true, true, true, true, true }, 
			{ false, false, true, false, false, false }, 
			{ false, false, true, false, false, false } };
	
	private static World theWorld;
	private static World theWorld2;
	
	@Before
	public void setUp() throws Exception {
		random = new Random(7357);
		theWorld = new World(6.0, 6.0, passableMap, random);
		theWorld2 = new World(6.0, 6.0, passableMap, random);
		theWorld2.addNewCreatedWorm(1.0, 1.5, 0.2, 0.5, "TheWorm", null);
		theWorld2.addNewCreatedWorm(2.0, 1.5, 0.2, 0.5, "TheWorm2", null);
		theWorld2.addNewFood();
		theWorld2.startGame();
	}
	
	@Test
	public void testConstructor() {
		World theWorld = new World(6.0, 6.0, passableMap, random);
		assertEquals(6.0, theWorld.getWidth(), EPS);
		assertEquals(6.0, theWorld.getHeight(), EPS);
		assertArrayEquals(passableMap, theWorld.getPassableMap());
		assertEquals(1.0, theWorld.getPPMWidth(), EPS);
		assertEquals(1.0, theWorld.getPPMHeight(), EPS);
		assertEquals(random, theWorld.getRandomGenerator());
	}

	@Test
	public void testConstructor_NotNull() {
		World theWorld = new World(5.0, 5.0, passableMap, random);
		assertNotNull(theWorld);
	}
	
	@Test
	public void testGetWidth() {
		assertEquals(6, theWorld.getWidth(), EPS);
	}
	
	@Test
	public void testGetHeight() {
		assertEquals(6, theWorld.getHeight(), EPS);
	}
	
	@Test
	public void testGetPassableMap() {
		assertArrayEquals(passableMap, theWorld.getPassableMap());
	}
	
	@Test
	public void testGetRandomGenerator() {
		assertEquals(random, theWorld.getRandomGenerator());
	}
	
	@Test
	public void testGetPPMHeight() {
		assertEquals(1, theWorld.getPPMHeight(), EPS);
	}
	
	@Test
	public void testGetPPMWidth() {
		assertEquals(1, theWorld.getPPMWidth(), EPS);
	}
	
	@Test
	public void testAddEmptyTeam() {
		theWorld.addEmptyTeam("TheTeam");
		assertEquals(1, theWorld.getTeams().size());
	}

	/*@Test
	public void addNewFood() {
		theWorld.addNewFood();
		assertEquals(1, theWorld.getFood().size());
	}*/
	
	/*@Test
	public void testAddNewCreatedFood() {
		fail("Not yet implemented");
	}*/
	
	@Test
	public void testRemoveFood() {
		theWorld2.removeFood(theWorld2.getFood().get(0));
		assertEquals(0, theWorld2.getFood().size());
	}
	
	/*@Test
	public void addNewWorm() {
		theWorld.addNewWorm();
		assertEquals(1, theWorld.getWorms().size());
	}*/
	
	/*@Test
	public void testAddNewCreatedWorm() {
		fail("Not yet implemented");
	}*/
	
	@Test
	public void testRemoveWorm() {
		theWorld2.removeWorm(theWorld2.getActiveWorms().get(0));
		assertEquals(1, theWorld2.getActiveWorms().size());
	}
	
	@Test
	public void testGetGameState() {
		assertEquals(GameState.INITIALISING, theWorld.getGameState());
	}
	
	@Test
	public void testIsOutsideWorldInside() {
		assertFalse(theWorld.isOutOfWorld(new Position(1.0,2.0)));
	}
	
	@Test
	public void testIsOutsideWorldTop() {
		assertTrue(theWorld.isOutOfWorld(new Position(1.0,10.0)));
	}
	
	@Test
	public void testIsOutsideWorldLeft() {
		assertTrue(theWorld.isOutOfWorld(new Position(-1.0,2.0)));
	}
	
	@Test
	public void testIsOutsideWorldRight() {
		assertTrue(theWorld.isOutOfWorld(new Position(10.0,2.0)));
	}
	
	@Test
	public void testIsOutsideWorldBottom() {
		assertTrue(theWorld.isOutOfWorld(new Position(1.0,-2.0)));
	}
	
	@Test
	public void testGetCurrentWorm() {
		assertEquals(theWorld2.getActiveWorms().get(0), theWorld2.getCurrentWorm());
	}
	
	@Test
	public void testStartNextTurn() {
		theWorld2.startNextTurn();
		assertFalse(theWorld2.getActiveWorms().get(0).getIsSelected());
		assertTrue(theWorld2.getActiveWorms().get(1).getIsSelected());
	}
	
	@Test
	public void testGetWinner() {
		theWorld2.removeWorm( theWorld2.getActiveWorms().get(0) );
		assertEquals("TheWorm2",theWorld2.getWinner());
	}
	
	@Test
	public void testIsImpassable_False() {
		assertFalse(theWorld.isImpassable(2.5, 3.26, 0.25));
	}
	
	@Test
	public void testIsImpassable_True() {
		assertTrue(theWorld.isImpassable(1.5, 1.26, 0.25));
	}
	
	@Test
	public void testCloseToImpassable_True() {
		assertTrue(theWorld.closeToImpassable(1.5, 2.25, 0.25));
	}
	
	@Test
	public void testCloseToImpassable_False() {
		assertFalse(theWorld.closeToImpassable(2.5, 4.26, 0.25));
	}
	
	@Test
	public void testIsAdjacent() {
		assertTrue(theWorld.isAdjacent(1.5, 2.25, 0.25));
	}
	
	@Test
	public void testIsAdjacent_TooFar() {
		assertFalse(theWorld.isAdjacent(2.5, 4.26, 0.25));
	}
	
	@Test
	public void testIsAdjacent_Impassable() {
		assertFalse(theWorld.isAdjacent(1.5, 1.26, 0.25));
	}
	
	// 
	@Test
	public void testImpassable() {
		boolean[][] blocks = new boolean[][] {
				{false, false, true, false, false},
				{true, true, true, true, false},
				{true, true, true, false, false},
				{true, true, true, true, true},
				{false, false, false, false, true}
		};
		World world = new World(20.0, 20.0, blocks, random);
		world.isAdjacent(4.0, 9.0, 0.35);
	}
}
