package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class WormsTest {

	private static final double EPS = Util.DEFAULT_EPSILON;
	
	private Random random;
	
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false, false, false, false, false, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ true, true, true, true, true, true, true, true, false, false }, 
			{ false, false, false, false, false, false, false, false, false, false }, 
			{ false, false, false, false, false, false, false, false, false, false }, 
			{ false, false, false, false, false, false, false, false, false, false } };
	
	private static World theWorld;
	private static World theWorld2;
	private static Worm theWorm;
	private static Worm theWorm2;
	
	@Before
	public void setUp() throws Exception {
		random = new Random(7357);
		theWorld = new World(10.0, 10.0, passableMap, random);
		theWorm = new Worm(theWorld, 1.0, 1.5, 0.2, 0.5, "TheWorm", null);
		theWorld2 = new World(10.0, 10.0, passableMap, random);
		theWorld2.addNewCreatedWorm(6.0, 4.0, 0.2, 0.25, "TheWorm", null);
		theWorld2.addNewCreatedWorm(6.0, 4.0, 0.2, 0.25, "TheWorm2", null);
		theWorld2.addNewCreatedFood(6.0, 4.0);
		theWorld2.startGame();
	}
	
	@Test
	public void testConstructor_NotNull() {
		Worm theWorm = new Worm(theWorld, 1.0, 1.5, 0.2, 0.5, "TheWorm", null);
		assertNotNull(theWorm);
	}
	
	@Test
	public void testConstructor_Rifle() {
		assertEquals(theWorm.getWormProjectiles().get(0).getName(), "Rifle");
	}
	
	@Test
	public void testConstructor_Bazooka() {
		assertEquals(theWorm.getWormProjectiles().get(1).getName(), "Bazooka");
	}
	
	@Test
	public void testSetX() {
		theWorm.setX(2);
		assertEquals(2, theWorm.getX(), EPS);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetX_IsNaN() {
		theWorm.setX(Double.NaN);
	}
	
	@Test
	public void testGetX() {
		assertEquals(1, theWorm.getX(), EPS);
	}
	
	@Test
	public void testSetY() {
		theWorm.setY(3);
		assertEquals(3, theWorm.getY(), EPS);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetY_IsNaN() {
		theWorm.setY(Double.NaN);
	}
	
	@Test
	public void testGetY() {
		assertEquals(1.5, theWorm.getY(), EPS);
	}
	
	@Test
	public void testSetPosition() {
		Position position = new Position(2.0,3.5);
		theWorm.setPosition(position);
		assertTrue(theWorm.getPosition().isEqual(position));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetPosition_IsNull() {
		theWorm.setPosition(null);
	}
	
	@Test
	public void testSetRadius_SingleCase() {
		theWorm.setRadius(0.75);
		assertEquals(0.75, theWorm.getRadius(), EPS);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetRadius_RadiusTooSmall() {
		theWorm.setRadius(0.2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetRadius_NaN() {
		theWorm.setRadius(Double.NaN);
	}
	
	@Test
	public void testGetRadius() {
		assertEquals(0.5,theWorm.getRadius(), EPS);
	}
	
	@Test
	public void testIsValidRadius_TrueCase() {
		assertTrue(theWorm.isValidRadius(0.75));
	}
	
	@Test
	public void testIsValidRadius_LowerBound() {
		assertTrue(theWorm.isValidRadius(theWorm.getMinimumRadius()));
	}
	
	@Test
	public void testIsValidRadius_UpperBound() {
		assertFalse(theWorm.isValidRadius(Double.POSITIVE_INFINITY));
	}
	
	@Test
	public void testIsValidRadius_IsNaN() {
		assertFalse(theWorm.isValidRadius(Double.NaN));
	}
	
	@Test
	public void testIsValidRadius_TooSmall() {
		assertFalse(theWorm.isValidRadius(0.1));
	}
	
	@Test
	public void testGetWorld() {
		assertEquals(theWorld, theWorm.getWorld());
	}
	
	@Test
	public void testIsOutsideWorldInside() {
		assertFalse(theWorm.isOutsideWorld());
	}
	
	@Test
	public void testIsOutsideWorldTop() {
		theWorm.setPosition( new Position(1.0, 20.0));
		assertTrue(theWorm.isOutsideWorld());
	}
	
	@Test
	public void testIsOutsideWorldLeft() {
		theWorm.setPosition( new Position(-1.0, 1.5));
		assertTrue(theWorm.isOutsideWorld());
	}
	
	@Test
	public void testIsOutsideWorldRight() {
		theWorm.setPosition( new Position(20.0, 1.5));
		assertTrue(theWorm.isOutsideWorld());
	}
	
	@Test
	public void testIsOutsideWorldBottom() {
		theWorm.setPosition( new Position(1.0, -1.0));
		assertTrue(theWorm.isOutsideWorld());
	}
	
	@Test
	public void testIsActive_TrueCase() {
		assertTrue(theWorm.isActive());
	}
	
	@Test
	public void testIsActive_FalseCase() {
		theWorm.DIE();
		assertFalse(theWorm.isActive());
	}
	
	@Test
	public void testDIE() {
		theWorm.DIE();
		assertFalse(theWorm.isActive());
	}
	
	@Test
	public void testSetMass_SingleCase() {
		theWorm.setMass();
		assertEquals(556.0618997,theWorm.getMass(), EPS);
	}
	
	@Test
	public void testGetMass() {
		assertEquals(556.0618997, theWorm.getMass(), EPS);
	}
	
	@Test
	public void testSetDirection_SingleCase() {
		theWorm.setDirection(0.7);
		assertEquals(0.7, theWorm.getDirection(), EPS);
	}
	
	@Test
	public void testGetDirection() {
		assertEquals(0.2, theWorm.getDirection(), EPS);
	}
	
	@Test
	public void testRestrictDirection_SingleCase() {
		assertEquals(Math.PI/2.0, Worm.restrictDirection(Math.PI/2.0),EPS);
	}
	
	@Test
	public void testRestrictDirection_TooBig() {
		assertEquals(Math.PI/2.0, (Worm.restrictDirection(9.0 * Math.PI / 2.0)), EPS);
	}
	
	@Test
	public void testRestrictDirection_TooSmall() {
		assertEquals(Math.PI/2.0, (Worm.restrictDirection(7.0 * - Math.PI / 2.0)), EPS);
	}
	
	@Test
	public void testRestrictDirection_LowerBound() {
		assertEquals(0,Worm.restrictDirection(0),EPS);
	}
	
	@Test
	public void testRestrictDirection_UpperBound() {
		assertEquals(0,Worm.restrictDirection(Math.PI * 2),EPS);
	}
	
	@Test
	public void testIsValidDirection_SingleCase() {
		assertTrue(theWorm.isValidDirection(0.2));
	}
	
	@Test
	public void testIsValidDirection_TooSmall() {
		assertFalse(theWorm.isValidDirection(-0.2));
	}
	
	@Test
	public void testIsValidDirection_TooBig() {
		assertFalse(theWorm.isValidDirection(10));
	}
	
	@Test
	public void testIsValidDirection_LowerBound() {
		assertTrue(theWorm.isValidDirection(0));
	}
	
	@Test
	public void testIsValidDirection_UpperBound() {
		assertTrue(theWorm.isValidDirection(2 * Math.PI));
	}
	
	@Test
	public void testSetSizeAspects() {
		theWorm.setSizeAspects(0.75);
		assertEquals(0.75, theWorm.getRadius(), EPS);
		assertEquals(1876.708911,theWorm.getMass(), EPS);
		assertEquals(1877,theWorm.getMaximumActionPoints());
		assertEquals(1877,theWorm.getMaximumHP());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetSizeAspects_TooSmall() {
		theWorm.setSizeAspects(0.2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetSizeAspects_NaN() {
		theWorm.setSizeAspects(Double.NaN);
	}
	
	@Test
	public void testGetMaximumActionPoints() {
		assertEquals(556, theWorm.getMaximumActionPoints());
	}

	@Test
	public void testGetActionPoints() {
		assertEquals(556, theWorm.getActionPoints(), EPS);
	}

	@Test
	public void testSetActionPoints_SingleCase() {
		theWorm.setActionPoints(250);
		assertEquals(250, theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_TooLarge() {
		theWorm.setActionPoints(1000);
		assertEquals(theWorm.getMaximumActionPoints(), theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_TooSmall() {
		theWorm2 = theWorld2.getActiveWorms().get(0);
		theWorm2.setActionPoints(-10);
		assertEquals(0, theWorm2.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_UpperBound() {
		theWorm.setActionPoints(theWorm.getMaximumActionPoints());
		assertEquals(theWorm.getMaximumActionPoints(), theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_LowerBound() {
		theWorm2 = theWorld2.getActiveWorms().get(0);
		theWorm2.setActionPoints(0);
		assertEquals(0, theWorm2.getActionPoints());
	}

	@Test
	public void testGetMaximumHP() {
		assertEquals(556, theWorm.getMaximumHP(), EPS);
	}

	@Test
	public void testGetHitPoints() {
		assertEquals(556, theWorm.getHitPoints(), EPS);
	}

	@Test
	public void testSetHitPoints_SingleCase() {
		theWorm.setHitPoints(250);
		assertEquals(250, theWorm.getHitPoints());
	}
	
	@Test
	public void testSetHitPoints_TooLarge() {
		theWorm.setHitPoints(1000);
		assertEquals(theWorm.getMaximumHP(), theWorm.getHitPoints());
	}
	
	@Test
	public void testSetHitPoints_TooSmall() {
		theWorm2 = theWorld2.getActiveWorms().get(0);
		theWorm2.setHitPoints(-10);
		assertEquals(0, theWorm2.getHitPoints());
	}
	
	@Test
	public void testSetHitPoints_UpperBound() {
		theWorm.setActionPoints(theWorm.getMaximumHP());
		assertEquals(theWorm.getMaximumHP(), theWorm.getHitPoints());
	}
	
	@Test
	public void testSetHitPoints_LowerBound() {
		theWorm2 = theWorld2.getActiveWorms().get(0);
		theWorm2.setHitPoints(0);
		assertEquals(0, theWorm2.getHitPoints());
	}
	
	@Test
	public void testSetName_SingleCase() {
		theWorm.setName("Stijn's \"friends\"");
		assertEquals("Stijn's \"friends\"", theWorm.getName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetName_smallStart() {
		theWorm.setName("stijn");
	}
	
	@Test
	public void testGetName() {
		assertEquals("TheWorm", theWorm.getName());
	}

	@Test
	public void testIsValidName_TrueCase() {
		assertTrue(theWorm.isValidName("Stijn's \"FrieNds\""));
	}
	
	@Test
	public void testIsValidName_SmallFirst() {
		assertFalse(theWorm.isValidName("stijn"));
	}
	
	@Test
	public void testIsValidName_ExclMark() {
		assertFalse(theWorm.isValidName("Stijn!"));
	}
	
	@Test
	public void testIsValidName_TooShort() {
		assertFalse(theWorm.isValidName("S"));
	}
	
	@Test
	public void testIsValidName_CharFirst() {
		assertFalse(theWorm.isValidName("'stijn'"));
	}
	
	@Test
	public void testGetIsSelected() {
		assertFalse(theWorm.getIsSelected());
	}

	@Test
	public void testSetIsSelected() {
		theWorm.setIsSelected(true);
		assertTrue(theWorm.getIsSelected());
	}
	
	@Test
	public void testCanTurn_TrueCase() {
		assertTrue(theWorm.canTurn(1));
	}
	
	@Test
	public void testCanTurn_TooMuch() {
		assertFalse(theWorm.canTurn(10000));
	}

	@Test
	public void testIsValidTurnAngle_TrueCase() {
		assertTrue(theWorm.isValidTurnAngle(1));
	}
	
	@Test
	public void testIsValidTurnAngle_isNaN() {
		assertFalse(theWorm.isValidTurnAngle(Double.NaN));
	}
	
	@Test
	public void testIsValidTurnAngle_PositiveInfinity() {
		assertFalse(theWorm.isValidTurnAngle(Double.POSITIVE_INFINITY));
	}
	
	@Test
	public void testIsValidTurnAngle_NegativeInfinity() {
		assertFalse(theWorm.isValidTurnAngle(Double.NEGATIVE_INFINITY));
	}

	@Test
	public void testTurn_SingleCaseDirection() {
		theWorm.turn(0.3);
		assertEquals(0.5,theWorm.getDirection(),EPS);
	}
	
	@Test
	public void testTurn_SingleCaseActionPoints() {
		theWorm.turn(0.3);
		assertEquals(553,theWorm.getActionPoints(),EPS);
	}

	@Test
	public void testCalculateTurnCost_SingleCase() {
		assertEquals(3,theWorm.calculateTurnCost(0.3));
	}
	
	@Test
	public void testCalculateForce() {
		assertEquals(8233.104429, theWorm.calculateForce(), EPS);
	}
	
	@Test
	public void testSetTeam() {
		Team theTeam = new Team(theWorld, "TheTeam");
		theWorm.setTeam(theTeam);
		assertEquals(theTeam, theWorm.getTeam());
	}
	
	@Test
	public void testGetTeam() {
		Team theTeam = new Team(theWorld, "TheTeam");
		theWorm.setTeam(theTeam);
		assertEquals(theTeam, theWorm.getTeam());
	}
	
	@Test
	public void testGetTeamName() {
		Team theTeam = new Team(theWorld, "TheTeam");
		theWorm.setTeam(theTeam);
		assertEquals("TheTeam", theWorm.getTeamName());
	}
	
	@Test
	public void testSelectNextWeapon() {
		theWorm.selectNextWeapon();
		assertEquals("Bazooka", theWorm.getSelectedWeapon().getName());
	}
	
	@Test
	public void testGetNameSelectedWeapon() {
		assertEquals("Rifle", theWorm.getSelectedWeapon().getName());
	}
	
	@Test
	public void testGetSelectedWeapon() {
		assertEquals("Rifle", theWorm.getSelectedWeapon().getName());
	}
	
	@Test
	public void testCalculateProjectilePosition() {
		Position position = theWorm.calculateProjectilePosition(theWorm.getWormProjectiles().get(0));
		Position expectedPosition = new Position(1.49663811,1.60067);
		assertTrue(expectedPosition.isEqual(position));
	}
	
	@Test
	public void testShoot_SingleCase() {
		theWorm.shoot(50);
		assertEquals(546, theWorm.getActionPoints());
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void testShoot_TooLittleActionPoints() {
		theWorm.setActionPoints(8);
		theWorm.shoot(50);
	}
	
	@Test
	public void testCanFall() {
		theWorm.setRadius(0.25);
		theWorm.setPosition( new Position(2.0, 6.0));
		assertTrue(theWorm.canFall());
	}
	
	@Test
	public void testFall() {
		theWorm.setRadius(0.25);
		theWorm.setPosition(new Position(2.0, 6.0));
		theWorm.fall();
		assertEquals(3.25, theWorm.getY(), EPS);
	}
	
	@Test
	public void testJumpDouble() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		worm.jump(0.0001);
		assertEquals(worm.getJumpStep( worm.getJumpTime(0.0001) ).getY(), worm.getY(), EPS);
		assertEquals(worm.getJumpStep( worm.getJumpTime(0.0001) ).getX(), worm.getX(), EPS);
	}
	
	@Test
	public void testJumpStepJumpable() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		assertEquals(6.00072731, worm.getJumpStep(0.0001).getX(), EPS);
		assertEquals(4.000147384, worm.getJumpStep(0.0001).getY(), EPS);
	}
	
	@Test
	public void testGetJumpTime() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		assertEquals(worm.getJumpTime(0.0001), 0.239, EPS);
	}

	@Test
	public void testCanMove() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		worm.setPosition(new Position(2.0, 5.0));
		worm.setRadius(0.25);
		assertTrue(worm.canMove());
	}

	@Test
	public void testMove() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		worm.setPosition(new Position(2.0, 5.0));
		worm.setRadius(0.25);
		worm.setDirection(0);
		worm.move();
		assertEquals(2.25, worm.getX(), EPS);
	}
	
	@Test
	public void testCanEat() {
		Worm worm = theWorld2.getActiveWorms().get(0);
		assertTrue(worm.canEat(theWorld2.getFood().get(0)));
	}
	
	@Test
	public void testEat(){
		Worm worm = theWorld2.getActiveWorms().get(0);
		worm.eat();
		assertEquals(0.275, worm.getRadius(), EPS);
		assertEquals(0, theWorld2.getFood().size(), EPS);
	}
	
}



