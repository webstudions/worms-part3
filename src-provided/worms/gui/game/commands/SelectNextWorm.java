package worms.gui.game.commands;

import worms.gui.game.PlayGameScreen;
import worms.gui.messages.MessageType;
import worms.model.IFacade;
import worms.model.ModelException;

public class SelectNextWorm extends InstantaneousCommand {

	public SelectNextWorm(IFacade facade, PlayGameScreen screen) {
		super(facade, screen);
	}

	@Override
	protected boolean canStart() {
		return true;
	}

	@Override
	protected void doStartExecution() {
		try {
			getFacade().startNextTurn(getWorld());
		} catch(ModelException exc) {
			getScreen().addMessage(exc.getMessage(), MessageType.ERROR);
		}
	}

}
