package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.*;
import worms.util.Util;

/**
 * @invar	The yield of this projectile is always valid, as specified in isValidYield().
 * 			| isValidYield(this.getYield())
 * @invar	The getDamage() method always returns a value bigger than 0.
 * @author 	nickscheynen
 * @author 	carmenvandeloo
 *
 */
public abstract class Projectile extends Jumpable implements Cloneable {
	
	/**
	 * Initializes this Projectile with the given world, position (x and y), radius and direction.
	 * @param 	world
	 * 			The world in which to place the created Projectile
	 * @param 	x
	 * 			The x coordinate for this Projectile in meters.
	 * @param 	y
	 * 			The y coordinate for this Projectile in meters.
	 * @param 	radius
	 * 			The radius of this Projectile in meters
	 * @param 	direction
	 * 			The direction this Projectile is facing in radians.
	 * @effect	This Projectile is initialized as a jumpable object with world as world, x as x, y as y, radius as radius and direction as direction.
	 * 			| super(world, x, y, radius, direction);
	 */
	@Raw
	public Projectile(World world, double x, double y, double radius, double direction) {
		super(world, x, y, radius, direction);
	}
	
	/**
	 * Returns the name of this projectile.
	 */
	@Basic
	public abstract String getName();
	
	/**
	 * Sets the yield of the projectile to the given yield.
	 * @param 	yield
	 * 			The yield with which this projectile will be fired. 
	 * @throws 	IllegalArgumentException
	 * 			| !isValid(yield)
	 */
	public void setYield(double yield) throws IllegalArgumentException {
		if(!isValidYield(yield))
			throw new IllegalArgumentException();
		this.yield = yield;
	}
	
	/**
	 * Returns the yield of this Projectile.
	 */
	@Basic
	protected double getYield() {
		return this.yield;
	}
	
	private double yield;
	
	/**
	 * Checks if the given yield is a number that lies between 0 and 100 (both included).
	 * @param 	yield
	 * 			The yield with which this projectile will be fired.
	 * @return 	Checks if the given yield is a number that lies between 0 and 100 (both included).
	 * 			| (!Double.isNaN(yield)) &&
	 *			| (Util.fuzzyGreaterThanOrEqualTo(yield, 0)) &&
	 *			| (Util.fuzzyLessThanOrEqualTo(yield, 100))
	 */
	public static boolean isValidYield(double yield) {
		return (!Double.isNaN(yield)) &&
				(Util.fuzzyGreaterThanOrEqualTo(yield, 0)) &&
				(Util.fuzzyLessThanOrEqualTo(yield, 100));
	}
	
	/**
	 * Calculates the force with which the jumpable object will jump.
	 * @return	Returns the force with which the jumpable object will jump.
	 */
	@Override
	public abstract double calculateForce();
	
	/**
	 * Calculate the radius of this projectile.
	 * @param 	mass
	 * 			The mass of the projectile in kilogram.
	 * @return 	Returns the radius that was calculated using the mass.
	 * 			| result == Math.pow( ( (mass/density) * (3.0/4.0) * (1.0/Math.PI) ) , (1.0/3.0))
	 */
	@Raw
	protected static double calculateRadius(double mass) {
		return Math.pow( ( (mass/density) * (3.0/4.0) * (1.0/Math.PI) ) , (1.0/3.0));
	}
	
	private static final double density = 7800;
	
	/**
	 * Makes this projectile jump.
	 * @param	timeStep
	 * 			The elementary time interval in which we may assume that the projectile will not completely move through a piece of impassable terrain.
	 * @effect	The method first calculates the force with which the projectile will be fired (using the calculateForce() method) and then uses the jump(timeStep, force) method from jumpable to jump.
	 * 			| double force = this.calculateForce();
	 * 			| super.jump(timeStep, force);
	 * @effect	If the projectile can hit a worm in its flight, it will hit the first one. 
	 * 			| if(this.canHitAny(this.getPosition())) { this.hit(); }
	 * @effect	After the jump the projectile's active state is set to false using the GameObject's DIE() method.
	 * 			| this.DIE()
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(timeStep)
	 * @throws 	UnsupportedOperationException
	 * 			If jump(timeStep, force) from Jumpable throws an UnsupportedOperationException.
	 */
	@Override
	public void jump(double timeStep) throws UnsupportedOperationException, IllegalArgumentException {
		if(Double.isNaN(timeStep)) 
			throw new IllegalArgumentException();
		if (Double.isNaN(timeStep))
			throw new IllegalArgumentException();
		try {
			double force = this.calculateForce();
			super.jump(timeStep, force);
			if(this.canHitAny(this.getPosition())) {
				this.hit();
			}
			this.DIE();
		} catch(UnsupportedOperationException uOExc) {
			throw uOExc;
		}
	}
	
	/**
	 * Returns the damage this projectile can inflict.
	 */
	@Basic
	public abstract int getDamage();
	
	/**
	 * Returns the action points needed to shoot this projectile.
	 */
	@Basic
	public abstract int getActionPointsNeeded();
	
	/**
	 * Sets the active state of this projectile to false.
	 * @post	Sets the active state of this projectile to false.
	 * 			| new.getActive() == false
	 */
	public void deactivate() {
		this.active = false;
	}
	
	/**
	 * Sets the active state of this projectile to true.
	 * @post	Sets the active state of this projectile to true.
	 * 			| new.getActive() == true
	 */
	public void activate() {
		this.active = true;
	}
	
	/**
	 * Returns the total amount of time (in seconds) that a jump of the given jumpable object would take. 
	 * @param	timeStep
	 * 			The elementary time interval in which we may assume that the jumpable object will not completely move through a piece of impassable terrain.
	 * @return	Returns the total amount of time (in seconds) that a jump of the given jumpable object would take. 
	 * 			The method increases the jumpTime with the given timeStep and checks if the position on that moment is either adjacent or outside the world.
	 * 			If this is not the case it checks if a worm can be hit. If any of the three conditions are true it stops and returns the jump time.
	 * 			| double jumpTime = timeStep;
	 * 			| Position jumpStep = getJumpStep(jumpTime);
	 * 			| if(!this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius())) {
	 * 			| 	while(!( (this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius()) || (this.getWorld().isOutOfWorld(jumpStep))) ) ) {
	 * 			|		if(this.canHitAny(jumpStep)) { return jumpTime; } else { jumpTime = jumpTime + timeStep; jumpStep = getJumpStep(jumpTime); }
	 * 			| } else { return 0.0; }
	 * 			| return jumpTime;
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(timeStep)
	 */
	@Override
	public double getJumpTime(double timeStep) throws IllegalArgumentException {
		
		if(Double.isNaN(timeStep)) 
			throw new IllegalArgumentException();
		
		double jumpTime = timeStep;
		
		Position jumpStep = getJumpStep(jumpTime);
		
		if(!this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius())) {
			while(!( 
					(this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius())
					|| (this.getWorld().isOutOfWorld(jumpStep))) ) 
				) {
					if(this.canHitAny(jumpStep)) {
						return jumpTime;
					} else {
						jumpTime = jumpTime + timeStep;
						jumpStep = getJumpStep(jumpTime);
					}
			};
		} else {
			return 0.0;
		}
		return jumpTime;
		
	};
	
	/**
	 * If this projectile can hit any of the active worms in the World. It will descrease it's hit points with the appropriate damage of this projectile.
	 * @effect	If this projectile can hit any of the active worms in the World. It will decreases it's hit points using the setHitPoints method and the appropriate damage for this projectile.
	 * 			| if( this.canHit(worldWorms.get(i), this.getPosition())) {
	 *			|	worldWorms.get(i).setHitPoints( worldWorms.get(i).getHitPoints() - this.getDamage() );
	 *			| }
	 */
	public void hit() {
		ArrayList<Worm> worldWorms = this.getWorld().getActiveWorms();
		for(int i=0; i < worldWorms.size(); i++) {
			if( this.canHit(worldWorms.get(i), this.getPosition())) {
				worldWorms.get(i).setHitPoints( worldWorms.get(i).getHitPoints() - this.getDamage() );
			}
		}
	}
	
	/**
	 * Checks if this projectile can hit any of the active worms on the given position.
	 * @param	projectilePosition
	 * 			The position for which we check if the projectile partially overlaps with an active worm.
	 * @return	Returns if this projectile can hit any of the active worms on the given position.	
	 * 			| if( this.canHit(worldWorms.get(i), projectilePosition)) { result = true; } else { result = false; }
	 */
	public boolean canHitAny(Position projectilePosition) {
		boolean result = false;
		ArrayList<Worm> worldWorms = this.getWorld().getActiveWorms();
		for(int i=0; i < worldWorms.size(); i++) {
			if( this.canHit(worldWorms.get(i), projectilePosition)) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * Returns true if the projectile and the worm are no further apart then the sum of their radius's.
	 * @param 	worm
	 * 			The worm for which we check if the projectile can hit it.
	 * @param 	projectilePosition
	 * 			The position for which we check if the projectile can hit the worm.
	 * @return	Returns true if the projectile and the worm are no further apart then the sum of their radiuses
	 * 			| double distance = Math.sqrt( Math.pow((worm.getX() - projectilePosition.getX()), 2) + Math.pow((worm.getY() - projectilePosition.getY()), 2) );
	 * 			| return distance < (worm.getRadius() + this.getRadius());
	 */
	public boolean canHit(Worm worm, Position projectilePosition) {
		double distance = Math.sqrt( Math.pow((worm.getX() - projectilePosition.getX()), 2) + Math.pow((worm.getY() - projectilePosition.getY()), 2) );
		return distance < (worm.getRadius() + this.getRadius());
	}
	
	/**
	 * Returns a Projectile that is identical to this Projectile.
	 * @return	new Projectile(this.getWorld(), this.getX(), this.getY(), this.getRadius(), this.getYield(), this.getDirection());
	 */
	@Override
	public Projectile clone() {
    	try {
			return (Projectile) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
    }
	
}
