package worms.model.programs.expressions;

import worms.model.programs.types.DoubleType;
import worms.util.Util;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class NumeralResultExpression extends Expression {

	public NumeralResultExpression(int line, int column) {
		super(line, column);
	}
	
	public abstract double getResult() throws IllegalArgumentException;
	
	public int getIntResult() throws IllegalArgumentException {
		try {
			long longInt = Math.round(Math.floor( this.getResult() ));
			if (longInt >= Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			} else if(longInt <= Integer.MIN_VALUE) {
				return Integer.MIN_VALUE;
			} else {
				return (int) longInt;
			}
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}
	
	@Override
	public boolean isEqual(Expression e) {
		if(e instanceof NumeralResultExpression) {
			return Util.fuzzyEquals(this.getResult(), ((NumeralResultExpression)e).getResult());
		} else if(e instanceof Variable) {
			if( ((Variable)e).getReturnType() instanceof DoubleType ) {
				return Util.fuzzyEquals(this.getResult(), ((Variable)e).getNumeralResult().getResult());
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public String getString() {
		return Double.toString( this.getResult() );
	}

}
