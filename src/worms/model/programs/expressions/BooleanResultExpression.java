package worms.model.programs.expressions;

import worms.model.programs.types.BooleanType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class BooleanResultExpression extends Expression {

	public BooleanResultExpression(int line, int column) {
		super(line, column);
	}
	
	public abstract boolean getResult();
	
	@Override
	public boolean isEqual(Expression e) {
		if(e instanceof BooleanResultExpression) {
			return (this.getResult() == ((BooleanResultExpression)e).getResult());
		} else if(e instanceof Variable) {
			if( ((Variable)e).getReturnType() instanceof BooleanType ) {
				return (this.getResult() == ((Variable)e).getBooleanResult().getResult());	
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public String getString() {
		return Boolean.toString( this.getResult() );
	}

}
