package worms.model.programs.expressions;

import worms.model.programs.Program;
import worms.model.programs.expressions.booleanresult.singular.BooleanConstant;
import worms.model.programs.expressions.entity.EntityExpression;
import worms.model.programs.expressions.entity.NullEntity;
import worms.model.programs.expressions.numeralresult.singular.DoubleConstant;
import worms.model.programs.types.BooleanType;
import worms.model.programs.types.DoubleType;
import worms.model.programs.types.EntityType;
import worms.model.programs.types.Type;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Variable extends Expression {

	public Variable(int line, int column, String name, Program program) {
		super(line, column);
		this.name = name;
		this.program = program;
	}
	
	private Program program;
	private String name;
	
	public String getName() {
		return this.name;
	}
	
	public Type getReturnType() {
		return program.getGlobals().get(name);
	}
	
	public BooleanConstant getBooleanResult() {
		return (BooleanConstant)this.program.getVar(name);
	}
	
	public DoubleConstant getNumeralResult() {
		return (DoubleConstant)this.program.getVar(name);
	}
	
	public EntityExpression getEntityResult() {
		if(this.program.getVar(name) == null)
			return new NullEntity(0, 0, this.program);
		else
			return (EntityExpression)this.program.getVar(name);
	}

	@Override
	public String getString() {
		if(this.getReturnType() instanceof BooleanType) {
			return this.getBooleanResult().getString();
		} else if(this.getReturnType() instanceof DoubleType) {
			return this.getNumeralResult().getString();
		} else if(this.getReturnType() instanceof EntityType) {
			return this.getEntityResult().getString();
		} else {
			return "No valid type.";
		}
	}

	@Override
	public boolean isEqual(Expression e) {
		if(e instanceof BooleanResultExpression)
			return ((BooleanResultExpression)e).isEqual(this);
		else if(e instanceof NumeralResultExpression)
			return ((NumeralResultExpression)e).isEqual(this);
		else if(e instanceof EntityExpression)
			return ((EntityExpression)e).isEqual(this);
		else if(e instanceof Variable) {
			if( ((Variable)e).getReturnType() instanceof BooleanType )
				return ((Variable)e).getBooleanResult().isEqual(this.getBooleanResult());
			else if( ((Variable)e).getReturnType() instanceof DoubleType )
				return ((Variable)e).getNumeralResult().isEqual(this.getNumeralResult());
			else if( ((Variable)e).getReturnType() instanceof EntityType )
				return ((Variable)e).getEntityResult().isEqual(this.getEntityResult());
			else 
				return false;
			}
		else 
			return false;
	}
	

}
