package worms.model.programs.expressions.numeralresult.binary;

import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.DoubleType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class NumeralResultBinaryExpression extends NumeralResultExpression {

	public NumeralResultBinaryExpression(int line, int column, Expression e1, Expression e2) {
		super(line, column);
		this.e1 = e1;
		this.e2 = e2;
	}
	
	protected final Expression e1;
	protected final Expression e2;
	
	public double getOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof DoubleType ) 
				return ((Variable)e).getNumeralResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof NumeralResultExpression)
				return ((NumeralResultExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}

}
