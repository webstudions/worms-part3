package worms.model.programs.expressions.numeralresult.binary;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Multiply extends NumeralResultBinaryExpression {

	public Multiply(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
	}
	
	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			return this.getOperand(e1) * this.getOperand(e2);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
		
	}
}

