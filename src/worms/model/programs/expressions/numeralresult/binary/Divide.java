package worms.model.programs.expressions.numeralresult.binary;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Divide extends NumeralResultBinaryExpression {

	public Divide(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
	}
	
	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			if(this.getOperand(e2) != 0.0 )
				return this.getOperand(e1) / this.getOperand(e2);
			else
				throw new IllegalArgumentException("Arithmetic exception: divide by zero.");
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
		
	}
}

