package worms.model.programs.expressions.numeralresult.singular;

import worms.model.Worm;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GetHP extends Getters {

	public GetHP(int line, int column, Expression e) {
		super(line, column, e);
	}

	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			if(this.getOperand(e) instanceof Worm) 
				return ((Worm)this.getOperand(e)).getHitPoints();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}

}

