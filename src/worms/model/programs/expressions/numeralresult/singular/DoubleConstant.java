package worms.model.programs.expressions.numeralresult.singular;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class DoubleConstant extends NumeralResultSingularExpression {

	public DoubleConstant(int line, int column, double d) {
		super(line, column);
		this.d = d;
	}
	
	public final double getResult() {
		return d;
	}
	
	private final double d;
}
