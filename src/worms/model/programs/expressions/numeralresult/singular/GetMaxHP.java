package worms.model.programs.expressions.numeralresult.singular;

import worms.model.Worm;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GetMaxHP extends Getters {

	public GetMaxHP(int line, int column, Expression e) {
		super(line, column, e);
	}

	@Override
	public double getResult() throws ArithmeticException {
		try {
			if(this.getOperand(e) instanceof Worm) 
				return ((Worm)this.getOperand(e)).getMaximumHP();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}

}