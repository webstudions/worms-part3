package worms.model.programs.expressions.numeralresult.singular;

import worms.model.GameObject;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GetRadius extends Getters {

	public GetRadius(int line, int column, Expression e) {
		super(line, column, e);
	}

	@Override
	public double getResult() throws ArithmeticException {
		try {
			if(this.getOperand(e) instanceof GameObject) 
				return ((GameObject)this.getOperand(e)).getRadius();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}

}

