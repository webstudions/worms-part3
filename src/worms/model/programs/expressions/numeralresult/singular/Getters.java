package worms.model.programs.expressions.numeralresult.singular;

import worms.model.GameObject;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.expressions.entity.EntityExpression;
import worms.model.programs.types.EntityType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class Getters extends NumeralResultSingularExpression {

	public Getters(int line, int column, Expression e) {
		super(line, column, e);
	}
	
	public GameObject getOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof EntityType ) 
				return ((Variable)e).getEntityResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof EntityExpression)
				return ((EntityExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}

	@Override
	public abstract double getResult();

}
