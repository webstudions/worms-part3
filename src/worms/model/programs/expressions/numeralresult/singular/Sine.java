package worms.model.programs.expressions.numeralresult.singular;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Sine extends Calculators {
	
	public Sine(int line, int column, Expression e) {
		super(line, column, e);
	}
	
	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			return Math.sin( this.getOperand(e) );
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
	}

}
