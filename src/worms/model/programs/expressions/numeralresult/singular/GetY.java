package worms.model.programs.expressions.numeralresult.singular;

import worms.model.GameObject;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GetY extends Getters {

	public GetY(int line, int column, Expression e) {
		super(line, column, e);
	}

	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			if(this.getOperand(e) instanceof GameObject) 
				return ((GameObject)this.getOperand(e)).getY();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}

}

