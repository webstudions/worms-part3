package worms.model.programs.expressions.numeralresult.singular;

import worms.model.programs.expressions.Expression;

public class Cosine extends Calculators {

	public Cosine(int line, int column, Expression e) {
		super(line, column, e);
	}
	
	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			return Math.cos( this.getOperand(e) );
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
		
	}

}
