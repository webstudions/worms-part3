package worms.model.programs.expressions.numeralresult.singular;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Sqrt extends Calculators {

	public Sqrt(int line, int column, Expression e) {
		super(line, column, e);
	}
	
	@Override
	public double getResult() throws IllegalArgumentException {
		try {
			if(this.getOperand(e) >= 0.0)
				return Math.sqrt( this.getOperand(e) );
			else 
				throw new IllegalArgumentException();
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
		
	}

}
