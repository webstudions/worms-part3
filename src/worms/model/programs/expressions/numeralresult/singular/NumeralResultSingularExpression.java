package worms.model.programs.expressions.numeralresult.singular;

import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class NumeralResultSingularExpression extends NumeralResultExpression {

	public NumeralResultSingularExpression(int line, int column) {
		super(line, column);
		this.e = null;
	}
	
	public NumeralResultSingularExpression(int line, int column, Expression e) {
		super(line, column);
		this.e = e;
	}
	
	protected final Expression e;
	
}
