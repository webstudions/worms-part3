package worms.model.programs.expressions.numeralresult.singular;

import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.DoubleType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class Calculators extends NumeralResultSingularExpression {

	public Calculators(int line, int column, Expression e) {
		super(line, column, e);
	}
	
	public double getOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof DoubleType ) 
				return ((Variable)e).getNumeralResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof NumeralResultExpression)
				return ((NumeralResultExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}

	@Override
	public abstract double getResult() throws IllegalArgumentException;

}
