package worms.model.programs.expressions.booleanresult.binary;

import worms.model.programs.expressions.BooleanResultExpression;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class BooleanResultBinaryExpression extends BooleanResultExpression {

	public BooleanResultBinaryExpression(int line, int column, Expression e1, Expression e2) {
		super(line, column);
	}
	
}