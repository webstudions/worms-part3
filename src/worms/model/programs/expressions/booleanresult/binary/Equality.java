package worms.model.programs.expressions.booleanresult.binary;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Equality extends BooleanResultBinaryExpression {

	public Equality(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
		this.e1 = e1;
		this.e2 = e2;
	}
	
	protected Expression e1;
	protected Expression e2;
	
	@Override
	public boolean getResult() {
		return e1.isEqual(e2);
	}

}