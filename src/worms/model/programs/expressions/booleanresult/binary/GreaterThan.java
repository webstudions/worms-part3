package worms.model.programs.expressions.booleanresult.binary;

import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GreaterThan extends BooleanResultNumeralInputBinaryExpression {

	public GreaterThan(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
	}
			
	public boolean getResult() throws IllegalArgumentException  {
		try {
			return this.getOperand(e1) > this.getOperand(e2);
		} catch (IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}
		
}