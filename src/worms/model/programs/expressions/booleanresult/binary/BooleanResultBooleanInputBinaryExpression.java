package worms.model.programs.expressions.booleanresult.binary;

import worms.model.programs.expressions.BooleanResultExpression;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.BooleanType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class BooleanResultBooleanInputBinaryExpression extends BooleanResultBinaryExpression {

	public BooleanResultBooleanInputBinaryExpression(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
		this.e1 = e1;
		this.e2 = e2;
	}
	
	protected final Expression e1;
	protected final Expression e2;
	
	public boolean getOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof BooleanType ) 
				return ((Variable)e).getBooleanResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof BooleanResultExpression)
				return ((BooleanResultExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}
}
