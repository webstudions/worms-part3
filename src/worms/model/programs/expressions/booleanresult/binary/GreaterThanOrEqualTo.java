package worms.model.programs.expressions.booleanresult.binary;

import worms.model.programs.expressions.Expression;
import worms.util.Util;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class GreaterThanOrEqualTo extends BooleanResultNumeralInputBinaryExpression {

	public GreaterThanOrEqualTo(int line, int column, Expression e1, Expression e2) {
		super(line, column, e1, e2);
	}

	@Override
	public boolean getResult() throws IllegalArgumentException {
		try {
			return Util.fuzzyGreaterThanOrEqualTo(this.getOperand(e1), this.getOperand(e2));
		} catch (IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		}
	}

}
