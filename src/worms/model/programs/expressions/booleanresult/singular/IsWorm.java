package worms.model.programs.expressions.booleanresult.singular;

import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.expressions.entity.WormEntity;
import worms.model.programs.types.EntityType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class IsWorm extends BooleanResultSingularExpression {

	public IsWorm(int line, int column, Expression e) {
		super(line, column);
		this.e = e;
	}
	
	private Expression e;

	@Override
	public boolean getResult() {
		Expression ent = null;
		if(e instanceof Variable)
			if(((Variable) e).getReturnType() instanceof EntityType)
				ent = ((Variable) e).getEntityResult();
		else 
			ent = e;
		
		if(ent instanceof WormEntity) {
			return true;
		} else {
			return false;
		}
	}

}