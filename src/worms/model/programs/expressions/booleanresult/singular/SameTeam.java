package worms.model.programs.expressions.booleanresult.singular;

import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.expressions.entity.WormEntity;
import worms.model.programs.types.EntityType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class SameTeam extends BooleanResultSingularExpression {

	public SameTeam(int line, int column, Expression e, Program program) {
		super(line, column);
		this.e = e;
		this.program = program;
	}
	
	private Expression e;
	private Program program;

	@Override
	public boolean getResult() {
		Expression ent = null;
		if(e instanceof Variable)
			if(((Variable)e).getReturnType() instanceof EntityType)
				ent = ((Variable) e).getEntityResult();
		else 
			ent = e;
		
		if(ent instanceof WormEntity) {
			if (program.getWorm().getTeam() == null) {
				return false;
			} else if (((WormEntity) ent).getResult().getTeam() == null) {
				return false;
			} else {
				return ((WormEntity) ent).getResult().getTeamName() == program.getWorm().getTeamName();
			}
		} else {
			return false;
		}
	}

}
