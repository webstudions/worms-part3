package worms.model.programs.expressions.booleanresult.singular;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class BooleanConstant extends BooleanResultSingularExpression {

	public BooleanConstant(int line, int column, boolean b) {
		super(line, column);
		this.b = b;
	}
	
	public final boolean getResult() {
		return b;
	}
	
	private final boolean b;
	
}
