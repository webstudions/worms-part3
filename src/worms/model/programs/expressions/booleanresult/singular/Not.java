package worms.model.programs.expressions.booleanresult.singular;

import worms.model.programs.expressions.BooleanResultExpression;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.BooleanType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Not extends BooleanResultSingularExpression {

	public Not(int line, int column, Expression e) {
		super(line, column);
		this.e = e;
	}
	
	protected final Expression e;
	
	public boolean getOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof BooleanType ) 
				return ((Variable)e).getBooleanResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof BooleanResultExpression)
				return ((BooleanResultExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}
	
	@Override
	public boolean getResult() throws IllegalArgumentException {
		try {
			return !this.getOperand(e);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException("Invalid program: type error.");
		}
	}

}
