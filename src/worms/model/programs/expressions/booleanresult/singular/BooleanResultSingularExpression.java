package worms.model.programs.expressions.booleanresult.singular;

import worms.model.programs.expressions.BooleanResultExpression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class BooleanResultSingularExpression extends BooleanResultExpression {

	public BooleanResultSingularExpression(int line, int column) {
		super(line, column);
	}

}
