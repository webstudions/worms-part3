package worms.model.programs.expressions.entity;

import java.util.ArrayList;

import worms.model.Food;
import worms.model.GameObject;
import worms.model.Worm;
import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.DoubleType;
import worms.util.Util;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class SearchObj extends EntityExpression {

	public SearchObj(int line, int column, Expression e, Program program) {
		super(line, column, program);
		this.e = e;
	}
	
	private Expression e;
	
	@Override
	public GameObject getResult() throws IllegalArgumentException {
		
		double d = 0.0;
		if(e instanceof Variable) {
			if(((Variable) e).getReturnType() instanceof DoubleType) {
				d = ((Variable) e).getNumeralResult().getResult();
			}
		} else if(e instanceof NumeralResultExpression) {
			d = ((NumeralResultExpression)e).getResult();
		} else {
			throw new IllegalArgumentException("Invalid program: type error.");
		}
		
		Worm worm = this.getProgram().getWorm();
		GameObject nearestObject = null;
		double nearestObjectDistance = Double.POSITIVE_INFINITY;
		ArrayList<GameObject> gameObjectList = worm.getWorld().getGameObjects();
		for(GameObject gameObject : gameObjectList) {
			if(!gameObject.equals(worm)) {
				// Get coordinates and radius of the target.
				double targetX = gameObject.getX();
				double targetY = gameObject.getY();
				double targetR = gameObject.getRadius();
				// Get coordinates of the shooting worm.
				double wX = worm.getX();
				double wY = worm.getY();
				// Get x and y difference between target and shooter.
				double dX = targetX - wX;
				double dY = targetY - wY;
				// Calculate the slope between target and shooter.
				double slopeST = dY / dX;
				// Calculate the perpendicular slope.
				double slopeS = -(1.0/slopeST);
				// Calculate the coordinates of the point on the edge of the circle where the tangent line of the shooter touches it.
				double edge1WX;
				double edge1WY;
				if (slopeS == Double.POSITIVE_INFINITY) {
					edge1WX = targetX;
					edge1WY = targetY + targetR;
				} else if (slopeS == Double.NEGATIVE_INFINITY) {
					edge1WX = - targetX;
					edge1WY = - (targetY + targetR);
				} else {
					edge1WX = targetX + Math.sqrt( ( ( Math.pow(targetR, 2)) / (1 + Math.pow(slopeS, 2)) ) ); 
					edge1WY = targetY + slopeS * (edge1WX - targetX);
				}				
				// Calculate the slope of the line between the point on the edge of the target and the shooter.
				double slopeSTE = (edge1WY - wY) / (edge1WX - wX);
				// Calculate the difference in slope between the edge and the center of the target and the shooter.
				double dRico = Math.abs( slopeSTE - slopeST );
				// Calculate the slope of the line under the angle (direction + given expression).
				double slopeAngle = Math.tan(worm.getDirection()+d);
				// If the slope of the angle lies between the slopes with the edges of the target, the target can be hit in a straight line.
				if(Util.fuzzyBetween(Math.min(slopeST-dRico, slopeST+dRico), Math.max(slopeST-dRico, slopeST+dRico), slopeAngle )) {
					// If the hitable object is closer than the previously found object, select this one.
					if(Util.fuzzyLessThanOrEqualTo( Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2) ), nearestObjectDistance)) {
						if(Util.fuzzyBetween(0.0, Math.PI/2.0,worm.getDirection())) {
							if(Util.fuzzyGreaterThanOrEqualTo(targetX, wX) && Util.fuzzyGreaterThanOrEqualTo(targetY, wY)) {
								nearestObject = gameObject;
								nearestObjectDistance = Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2));
							}
						} else if(Util.fuzzyBetween(Math.PI/2.0, Math.PI, worm.getDirection())) {
							if(Util.fuzzyLessThanOrEqualTo(targetX, wX) && Util.fuzzyGreaterThanOrEqualTo(targetY, wY)) {
								nearestObject = gameObject;
								nearestObjectDistance = Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2));
							}
						} else if(Util.fuzzyBetween(Math.PI, (3.0*Math.PI)/2.0, worm.getDirection())) {
							if(Util.fuzzyLessThanOrEqualTo(targetX, wX) && Util.fuzzyLessThanOrEqualTo(targetY, wY)) {
								nearestObject = gameObject;
								nearestObjectDistance = Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2));
							}
						} else if(Util.fuzzyBetween((3.0*Math.PI)/2.0, 2.0*Math.PI, worm.getDirection())) {
							if(Util.fuzzyGreaterThanOrEqualTo(targetX, wX) && Util.fuzzyLessThanOrEqualTo(targetY, wY)) {
								nearestObject = gameObject;
								nearestObjectDistance = Math.sqrt( Math.pow(dX, 2) + Math.pow(dY, 2));
							}
						}		
					}
				}
			}
		}
		return nearestObject;
	}
	
	public EntityExpression getResultEntityExpression() {
		if(this.getResult() instanceof Worm) {
			WormEntity result = new WormEntity(0, 0, this.getProgram());
			result.setWorm((Worm)this.getResult());
			return result;
		} else if(this.getResult() instanceof Food) {
			FoodEntity result = new FoodEntity(0, 0, this.getProgram());
			result.setFood((Food)this.getResult());
			return result;
		} else {
			return new NullEntity(0, 0, this.getProgram());
		}
	}

	@Override
	public String getString() {
		if(this.getResult() == null)
			return "null.";
		if(this.getResult() instanceof Worm) 
			return ((Worm)this.getResult()).getName();
		else
			return "Food ration";
	}

}
