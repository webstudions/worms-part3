package worms.model.programs.expressions.entity;

import worms.model.Worm;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class WormEntity extends EntityExpression {

	public WormEntity(int line, int column, Program program) {
		super(line, column, program);
	}
	
	public void setWorm(Worm worm) {
		this.worm = worm;
	}
	
	protected Worm worm;

	@Override
	public Worm getResult() {
		return worm;
	}
	
	@Override
	public String getString() {
		return worm.getName();
	}

}
