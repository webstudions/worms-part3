package worms.model.programs.expressions.entity;

import worms.model.Worm;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class SelfEntity extends WormEntity {

	public SelfEntity(int line, int column, Program program) {
		super(line, column, program);
	}

	@Override
	public Worm getResult() {
		return this.getProgram().getWorm();
	}

	@Override
	public String getString() {
		return this.getProgram().getWorm().getName();
	}
}
