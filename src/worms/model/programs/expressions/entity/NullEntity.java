package worms.model.programs.expressions.entity;

import worms.model.GameObject;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class NullEntity extends EntityExpression {

	public NullEntity(int line, int column, Program program) {
		super(line, column, program);
	}

	@Override
	public GameObject getResult() {
		return null;
	}

	@Override
	public String getString() {
		return "null";
	}
	
	

}
