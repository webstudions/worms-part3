package worms.model.programs.expressions.entity;

import worms.model.Food;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class FoodEntity extends EntityExpression {

	public FoodEntity(int line, int column, Program program) {
		super(line, column, program);
	}

	public void setFood(Food food) {
		this.food = food;
	}
	
	protected Food food;

	@Override
	public Food getResult() {
		return food;
	}
	
	@Override
	public String getString() {
		return "Food Ration";
	}

}
