package worms.model.programs.expressions.entity;

import worms.model.GameObject;
import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.EntityType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class EntityExpression extends Expression {

	public EntityExpression(int line, int column, Program program) {
		super(line, column);
		this.program = program;
	}
	
	public Program getProgram() {
		return this.program;
	}
	
	private final Program program;
	
	public abstract GameObject getResult();
	
	@Override
	public boolean isEqual(Expression e) {
		if(e instanceof EntityExpression) {
			return this.getResult().equals( ((EntityExpression)e).getResult() );
		} else if(e instanceof Variable) {
			if( ((Variable)e).getReturnType() instanceof EntityType ) {
				if( ((Variable)e).getEntityResult().getResult() == null )
					return this.getResult() == null;
				else if(this.getResult() == null)
					return ((Variable)e).getEntityResult().getResult() == null;
				else 
					return this.getResult().equals( ((Variable)e).getEntityResult().getResult() );
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
