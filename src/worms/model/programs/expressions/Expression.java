package worms.model.programs.expressions;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
@SuppressWarnings("unused")
public abstract class Expression {
	
	public Expression(int line, int column) {
		this.line = line;
		this.column = column;
	}
	
	private final int line;
	private final int column;

	public abstract String getString();
	
	public abstract boolean isEqual(Expression e);
}
