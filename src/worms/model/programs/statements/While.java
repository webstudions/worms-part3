package worms.model.programs.statements;

import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class While extends BodyStatements {

	public While(int line, int column, Expression condition, Statement body, Program program) {
		super(line, column, program,body);
		this.condition = condition;
	}
	
	private final Expression condition;
	private boolean PausedLastTime;
	
	@Override
	public void execute() throws IllegalArgumentException {
		while ( Statement.getBooleanOperand(condition) == true && !this.getProgram().getPaused() ) {
			if (PausedLastTime) {
			} else {
				this.getProgram().setPosition(this.getLine(), this.getColumn());
			}
			this.getBody().execute();
			
			if ( this.getProgram().getPaused() ) {
				PausedLastTime = true;
			} else {
				PausedLastTime = false;
			}
		}
	}

}