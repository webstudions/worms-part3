package worms.model.programs.statements;

import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Print extends Statement {

	public Print(int line, int column, Expression e, Program program) {
		super(line, column, program);
		this.e = e;
	}
	
	private Expression e;

	@Override
	public void execute() {
		Expression exp = e;
		String string = exp.getString();
		System.out.println(string);
		this.getProgram().setPosition(this.getLine(), this.getColumn());
	}

	
}
