package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Move extends ActionStatements {

	public Move(int line, int column, Program program, IActionHandler handler) {
		super(line, column, program, handler);
	}

	@Override
	public void execute() throws IllegalArgumentException {
		Program program = this.getProgram();
		if(program.getWorm().canMove()) {
			program.setPosition(this.getLine(), this.getColumn());
			this.getHandler().move(program.getWorm());
		} else {
			program.setPause();
		}
	}


}
