package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class ToggleWeap extends ActionStatements {

	public ToggleWeap(int line, int column, Program program, IActionHandler handler) {
		super(line, column, program, handler);
	}

	@Override
	public void execute() {
		this.getProgram().setPosition(this.getLine(), this.getColumn());
		this.getHandler().toggleWeapon(this.getProgram().getWorm());
	}

	
}
