package worms.model.programs.statements;

import worms.model.programs.Program;
import worms.model.programs.expressions.BooleanResultExpression;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.expressions.booleanresult.singular.BooleanConstant;
import worms.model.programs.expressions.entity.EntityExpression;
import worms.model.programs.expressions.entity.SearchObj;
import worms.model.programs.expressions.numeralresult.singular.DoubleConstant;
import worms.model.programs.types.BooleanType;
import worms.model.programs.types.DoubleType;
import worms.model.programs.types.EntityType;
import worms.model.programs.types.Type;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Assignment extends Statement {

	public Assignment(int line, int column, String variableName, Expression rhs, Program program) {
		super(line, column, program);
		this.variableName = variableName;
		this.rhs = rhs;
	}
	
	private String variableName;
	private Expression rhs;
	
	public Type getGlobalType(String name) {
		return this.getProgram().getGlobals().get(name);
	}
 
	@Override
	public void execute() throws IllegalArgumentException {
		Expression value = null;
		Type valueType = null;
		if(rhs instanceof NumeralResultExpression) {
			value = new DoubleConstant( 0, 0, ((NumeralResultExpression) rhs).getResult()); 
			valueType = new DoubleType();
		} else if (rhs instanceof BooleanResultExpression) {
			value = new BooleanConstant( 0, 0, ((BooleanResultExpression) rhs).getResult());
			valueType = new BooleanType();
		} else if (rhs instanceof EntityExpression) {
			if(rhs instanceof SearchObj)
				value = ((SearchObj)rhs).getResultEntityExpression();
			else
				value = rhs;
			valueType = new EntityType();
		} else if (rhs instanceof Variable) {
			if( ((Variable) rhs).getReturnType() instanceof DoubleType ) {
				value = ((Variable) rhs).getNumeralResult();
				valueType = new DoubleType();
			} else if(((Variable) rhs).getReturnType() instanceof BooleanType ) {
				value = ((Variable) rhs).getBooleanResult();
				valueType = new BooleanType();
			} else if(((Variable) rhs).getReturnType() instanceof EntityType ) {
				if(rhs instanceof SearchObj) {
					value = ((SearchObj)rhs).getResultEntityExpression();
				}	
				else {
					value = this.getProgram().getVar(((Variable)rhs).getName());
				}
				valueType = new EntityType();
			}
		}
		
		if(valueType.getClass() != this.getGlobalType(variableName).getClass() )
			throw new IllegalArgumentException("Invalid Progam: type error."); 
		
		this.getProgram().setPosition(this.getLine(), this.getColumn());
		this.getProgram().putVar(variableName, value);
	}


}
