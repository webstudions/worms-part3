package worms.model.programs.statements;

import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class If extends Statement {

	public If(int line, int column, Expression condition, Statement then, Statement otherwise, Program program) {
		super(line, column, program);
		this.condition = condition;
		this.then = then;
		this.otherwise = otherwise;
	}
	
	private final Expression condition;
	private final Statement then;
	private final Statement otherwise;

	@Override
	public void execute() throws IllegalArgumentException {	
		if(Statement.getBooleanOperand(this.getCondition())) {
			this.getProgram().setPosition(this.getLine(), this.getColumn());
			this.getThen().execute();
		} else {
			this.getProgram().setPosition(this.getLine(), this.getColumn());
			this.getOtherwise().execute();
		}

	}

	public Statement getThen() {
		return then;
	}
	
	public Statement getOtherwise() {
		return otherwise;
	}
	
	public Expression getCondition() {
		return this.condition;
	}
}
