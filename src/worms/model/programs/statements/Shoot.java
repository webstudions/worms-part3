package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Shoot extends ActionStatements {

	public Shoot(int line, int column, Expression yield, Program program, IActionHandler handler) {
		super(line, column, program, handler);
		this.yield = yield;
	}
	
	private final Expression yield;

	@Override
	public void execute() throws IllegalArgumentException {
		Program program = this.getProgram();
		double value = Statement.getNumeralOperand(this.yield);

		long longYield = Math.round(value);
		int intYield;
		if (longYield >= Integer.MAX_VALUE) {
			intYield = Integer.MAX_VALUE;
		}
		else if (longYield <= Integer.MIN_VALUE) {
			intYield = Integer.MIN_VALUE;
		} else
			intYield = (int) longYield;
		
		if(program.getWorm().canShoot()) {
			program.setPosition(this.getLine(), this.getColumn());
			this.getHandler().fire(program.getWorm(), intYield);
		} else {
			program.setPause();
		}		
	}

}