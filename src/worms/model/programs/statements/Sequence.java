package worms.model.programs.statements;

import java.util.ArrayList;
import java.util.List;

import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Sequence extends Statement {

	public Sequence(int line, int column, List<Statement> statements, Program program) { 
		super(line, column, program);
		this.statements = statements;
	}
	
	protected final List<Statement> statements;
	
	private int i;
	
	@Override
	public void execute() throws IllegalArgumentException {
		List<Statement> statements = this.getStatements();
		if (statements.size() == 2) {
			getLineList();
			this.getProgram().setPosition(LineList.get(0)-1, 0);
			while( (i<statements.size()) && (!this.getProgram().getPaused()) ) {
				if( (this.getProgram().getLine()+1 == statements.get(0).line) ) {
					if ( statements.get(0) instanceof While ) {
						statements.get(0).execute();
						this.getProgram().setPosition(LineList.get(1)-1,0);
					}
					statements.get(0).execute();
					i++;
				} else if( statements.get(1) instanceof Sequence || statements.get(1) instanceof BodyStatements || statements.get(1) instanceof If) {
					statements.get(1).execute();
					i++;
				} else {
					statements.get(1).execute();
					i++;
					break;
				}
			}
			i = 0;
		}
	}

	public List<Statement> getStatements() {
		return this.statements;
	}
	
	public ArrayList<Integer> getLineList() {
		LineList.add(statements.get(0).getLine());
		LineList.add(statements.get(1).getLine());
		return LineList;
	}
	
	public ArrayList<Integer> LineList = new ArrayList<Integer>();
}
