package worms.model.programs.statements;

import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class BodyStatements extends Statement {

	public BodyStatements(int line, int column, Program program, Statement body) {
		super(line, column, program);
		this.body = body;

	}

	public Statement getBody() {
		return this.body;
	}

	private Statement body;
}
