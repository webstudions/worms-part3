package worms.model.programs.statements;

import worms.model.programs.Program;
import worms.model.programs.expressions.BooleanResultExpression;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.NumeralResultExpression;
import worms.model.programs.expressions.Variable;
import worms.model.programs.types.BooleanType;
import worms.model.programs.types.DoubleType;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class Statement {
	
	public Statement(int line, int column, Program program) {
		this.line = line;
		this.column = column;
		this.program = program;
	}
	
	public int getLine() {
		return line;
	}

	
	public int getColumn() {
		return column;
	}

	protected final int line;
	protected final int column;
	
	public Program getProgram() {
		return this.program;
	}
	
	public void setProgram(Program program) {
		this.program = program;
	}

	private Program program;
	
	public abstract void execute() throws IllegalArgumentException;
	
	public static boolean getBooleanOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable )
			if ( ((Variable)e).getReturnType() instanceof BooleanType ) 
				return ((Variable)e).getBooleanResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof BooleanResultExpression)
				return ((BooleanResultExpression)e).getResult();
			else
				throw new IllegalArgumentException("Invalid program: type error.");
	}
	
	public static double getNumeralOperand(Expression e) throws IllegalArgumentException {
		if (e instanceof Variable)
			if( ((Variable) e).getReturnType() instanceof DoubleType )
				return ((Variable) e).getNumeralResult().getResult();
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
		else
			if(e instanceof NumeralResultExpression)
				return (((NumeralResultExpression) e).getResult());
			else 
				throw new IllegalArgumentException("Invalid program: type error.");
	}
}
