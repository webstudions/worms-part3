package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;
import worms.model.programs.expressions.Expression;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Turn extends ActionStatements {

	public Turn(int line, int column, Expression angle, Program program, IActionHandler handler) {
		super(line, column, program, handler);
		this.angle = angle;
	}
	
	private final Expression angle;

	@Override
	public void execute() throws IllegalArgumentException {
		double value = Statement.getNumeralOperand(angle);
		
		if(this.getProgram().getWorm().canTurn(value)) {
			this.getProgram().setPosition(this.getLine(), this.getColumn());
			this.getHandler().turn(this.getProgram().getWorm(), value);
		} else {
			this.getProgram().setPause();
		}		
	}


}
