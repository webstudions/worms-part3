package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Jump extends ActionStatements {

	public Jump(int line, int column, Program program, IActionHandler handler) {
		super(line, column, program, handler);
	}

	@Override
	public void execute() throws IllegalArgumentException {
		Program program = this.getProgram();
		if(program.getWorm().getActionPoints() != 0.0) {
			program.setPosition(this.getLine(), this.getColumn());
			this.getHandler().jump(program.getWorm());
		} else {
			program.setPause();
		}	
	}


}