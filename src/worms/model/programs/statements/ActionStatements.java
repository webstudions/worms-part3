package worms.model.programs.statements;

import worms.gui.game.IActionHandler;
import worms.model.programs.Program;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public abstract class ActionStatements extends Statement {

	public ActionStatements(int line, int column, Program program, IActionHandler handler) {
		super(line, column, program);
		this.handler = handler;
	}
	
	public IActionHandler getHandler() {
		return this.handler;
	}
	
	private IActionHandler handler;

	@Override
	public abstract void execute() throws IllegalArgumentException;

}
