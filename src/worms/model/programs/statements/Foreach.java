package worms.model.programs.statements;

import java.util.ArrayList;

import worms.model.Food;
import worms.model.GameObject;
import worms.model.Worm;
import worms.model.programs.Program;
import worms.model.programs.ProgramFactory.ForeachType;
import worms.model.programs.expressions.entity.FoodEntity;
import worms.model.programs.expressions.entity.WormEntity;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Foreach extends BodyStatements {

	public Foreach(int line, int column, ForeachType type, String variableName, Statement body, Program program) {
		super(line, column, program, body);
		this.type = type;
		this.variableName = variableName;
	}
	
	private final ForeachType type;
	private final String variableName;
	
	@Override
	public void execute() throws IllegalArgumentException {
		Program program = this.getProgram();
		if(type == ForeachType.WORM) {
			ArrayList<Worm> activeWorldWorms = program.getWorm().getWorld().getActiveWorms();
			for(Worm worm : activeWorldWorms) {
				WormEntity wormEntity = new WormEntity(line, column, program);
				wormEntity.setWorm(worm);
				program.putVar(variableName, wormEntity);
				program.setPosition(this.getLine(), this.getColumn());
				this.getBody().execute();
			}
		} else if(type == ForeachType.FOOD) {
			ArrayList<Food> worldFood = program.getWorm().getWorld().getFood();
			for(Food food : worldFood) {
				FoodEntity foodEntity = new FoodEntity(line, column, program);
				foodEntity.setFood(food);
				program.putVar(variableName, foodEntity);
				program.setPosition(this.getLine(), this.getColumn());
				this.getBody().execute();
			}
		} else { //ANY
			ArrayList<GameObject> worldGameObjects = program.getWorm().getWorld().getGameObjects();
			for(GameObject gameObject : worldGameObjects) {
				if(gameObject instanceof Food) {
					FoodEntity foodEntity = new FoodEntity(line, column, program);
					foodEntity.setFood((Food)gameObject);
					program.putVar(variableName, foodEntity);
				} else {
					WormEntity wormEntity = new WormEntity(line, column, program);
					wormEntity.setWorm((Worm)gameObject);
					program.putVar(variableName, wormEntity);
				}
				program.setPosition(this.getLine(), this.getColumn());
				this.getBody().execute();
			}
		}
	}

}