package worms.model.programs.types;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class EntityType extends Type {

	public EntityType() {
		super("Entity");
	}

}