package worms.model.programs.types;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class BooleanType extends Type {

	public BooleanType() {
		super("Boolean");
	}

}