package worms.model.programs.types;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Type {

	public Type(String name) {
		this.name = name;
	}
	
	private String name;
	 
	public String getName() {
		return this.name;
	}
}
