package worms.model.programs.types;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class DoubleType extends Type {

	public DoubleType() {
		super("Double");
	}

}
