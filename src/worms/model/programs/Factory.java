package worms.model.programs;

import java.util.List;

import worms.model.programs.ProgramFactory;
import worms.model.programs.expressions.*;
import worms.model.programs.expressions.booleanresult.binary.*;
import worms.model.programs.expressions.booleanresult.singular.*;
import worms.model.programs.expressions.entity.*;
import worms.model.programs.expressions.numeralresult.binary.*;
import worms.model.programs.expressions.numeralresult.singular.*;
import worms.model.programs.statements.*;
import worms.model.programs.types.*;

public class Factory implements ProgramFactory<Expression, Statement, Type> {
	
	public void setProgram(Program program) {
		this.program = program;
	}
	
	public Program getProgram() {
		return this.program;
	}
	
	private Program program;

	/**
	 * Create an expression representing a double literal, with value d
	 */
	public Expression createDoubleLiteral(int line, int column, double d) {
		return new DoubleConstant(line, column, d);
	}

	/**
	 * Create an expression representing a boolean literal, with value b
	 */
	public Expression createBooleanLiteral(int line, int column, boolean b) {
		return new BooleanConstant(line, column, b);
	}

	/**
	 * Create an expression representing the logical and operation on two
	 * expressions e1 and e2
	 */
	public Expression createAnd(int line, int column, Expression e1, Expression e2) {
		return new And(line, column, e1, e2);
	}

	/**
	 * Create an expression representing the logical or operation on two
	 * expressions e1 and e2
	 */
	public Expression createOr(int line, int column, Expression e1, Expression e2) {
		return new Or(line, column, e1, e2);
	}

	/**
	 * Create an expression representing the logical not operation on the
	 * expression e
	 */
	public Expression createNot(int line, int column, Expression e) {
		return new Not(line, column, e);
	}

	/**
	 * Create an expression representing the literal 'null'
	 */
	public Expression createNull(int line, int column) {
		return new NullEntity(line, column, this.getProgram());
	}

	/**
	 * Create an expression representing a reference to the worm that is
	 * executing the program
	 */
	public Expression createSelf(int line, int column) {
		return new SelfEntity(line, column, this.getProgram());
	}

	/**
	 * Create an expression to get the x-coordinate of the entity identified by
	 * the expression e
	 */
	public Expression createGetX(int line, int column, Expression e) {
		return new GetX(line, column, e);
	}

	/**
	 * Create an expression to get the y-coordinate of the entity identified by
	 * the expression e
	 */
	public Expression createGetY(int line, int column, Expression e) {
		return new GetY(line, column, e);
	}

	/**
	 * Create an expression to get the radius of the entity identified by the
	 * expression e
	 */
	public Expression createGetRadius(int line, int column, Expression e) {
		return new GetRadius(line, column, e);
	}

	/**
	 * Create an expression to get the direction of the entity identified by the
	 * expression e
	 */
	public Expression createGetDir(int line, int column, Expression e) {
		return new GetDirection(line, column, e);
	}

	/**
	 * Create an expression to get the action points of the entity identified by
	 * the expression e
	 */
	public Expression createGetAP(int line, int column, Expression e) {
		return new GetAP(line, column, e);
	}

	/**
	 * Create an expression to get the maximum number of action points of the
	 * entity identified by the expression e
	 */
	public Expression createGetMaxAP(int line, int column, Expression e) {
		return new GetMaxAP(line, column, e);
	}

	/**
	 * Create an expression to get the hit points of the entity identified by
	 * the expression e
	 */
	public Expression createGetHP(int line, int column, Expression e) {
		return new GetHP(line, column, e);
	}

	/**
	 * Create an expression to get the maximum number of hit points of the
	 * entity identified by the expression e
	 */
	public Expression createGetMaxHP(int line, int column, Expression e) {
		return new GetMaxHP(line, column, e);
	}

	/**
	 * Create an expression to evaluate whether the worm identified by the
	 * expression expression belongs to the same team as the worm that is executing the
	 * program
	 */
	public Expression createSameTeam(int line, int column, Expression e) {
		return new SameTeam(line, column, e, this.getProgram());
	}

	/**
	 * Create an expression to get the closest object in the direction theta+e,
	 * starting from the position of the worm that is executing the program,
	 * where theta is the current direction of the worm that is executing the
	 * program
	 */
	public Expression createSearchObj(int line, int column, Expression e) {
		return new SearchObj(line, column, e, this.getProgram());
	}

	/**
	 * Create an expression that evaluates whether the entity identified by the
	 * expression expression is a worm
	 */
	public Expression createIsWorm(int line, int column, Expression e) {
		return new IsWorm(line, column, e);
	}

	/**
	 * Create an expression that evaluates whether the entity identified by the
	 * expression expression is a food ration
	 */
	public Expression createIsFood(int line, int column, Expression e) {
		return new IsFood(line, column, e);
	}

	/**
	 * Create an expression that evaluates to the value of the variable with the
	 * given name
	 */
	public Expression createVariableAccess(int line, int column, String name) {
		return new Variable(line, column, name, this.getProgram());
	}
	
	@Override
	public Expression createVariableAccess(int line, int column, String name, Type type) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * less than the value of the expression e2
	 */
	public Expression createLessThan(int line, int column, Expression e1, Expression e2) {
		return new LessThan(line, column, e1, e2);
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * greater than the value of the expression e2
	 */
	public Expression createGreaterThan(int line, int column, Expression e1, Expression e2) {
		return new GreaterThan(line, column, e1, e2);
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * less than or equal to the value of the expression e2
	 */
	public Expression createLessThanOrEqualTo(int line, int column, Expression e1, Expression e2) {
		return new LessThanOrEqualTo(line, column, e1, e2);
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * greater than or equal to the value of the expression e2
	 */
	public Expression createGreaterThanOrEqualTo(int line, int column, Expression e1, Expression e2) {
		return new GreaterThanOrEqualTo(line, column, e1, e2);
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * equal to the value of the expression e2
	 */
	public Expression createEquality(int line, int column, Expression e1, Expression e2) {
		return new Equality(line, column, (e1), (e2));
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * not equal to the value of the expression e2
	 */
	public Expression createInequality(int line, int column, Expression e1, Expression e2) {
		return new Inequality(line, column, (e1), (e2));
	}

	/**
	 * Create an expression that represents the addition of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createAdd(int line, int column, Expression e1, Expression e2) throws IllegalArgumentException {
		return new Add(line, column, e1, e2);
	}

	/**
	 * Create an expression that represents the subtraction of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createSubtraction(int line, int column, Expression e1, Expression e2) {
		return new Subtract(line, column, e1, e2);
	}

	/**
	 * Create an expression that represents the multiplication of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createMul(int line, int column, Expression e1, Expression e2) {
		return new Multiply(line, column, e1, e2);
	}

	/**
	 * Create an expression that represents the division of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createDivision(int line, int column, Expression e1, Expression e2) {
		return new Divide(line, column, e1, e2);
	}

	/**
	 * Create an expression that represents the square root of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createSqrt(int line, int column, Expression e) {
		return new Sqrt(line, column, e);
	}

	/**
	 * Create an expression that represents the sine of the value of expression
	 * e1 and the value of the expression e2
	 */
	public Expression createSin(int line, int column, Expression e) {
		return new Sine(line, column, e);
	}

	/**
	 * Create an expression that represents the cosine of the value of
	 * expression e1 and the value of the expression e2
	 */
	public Expression createCos(int line, int column, Expression e) {
		return new Cosine(line, column, e);
	}

	/* actions */

	/**
	 * Create a statement that represents a turn of the worm executing the
	 * program by the value of the angle expression
	 */
	public Statement createTurn(int line, int column, Expression angle) {
		return new Turn(line, column, angle, this.getProgram(), this.getProgram().getHandler());
	}

	/**
	 * Create a statement that represents a move of the worm executing the
	 * program
	 */
	public Statement createMove(int line, int column) {
		return new Move(line, column, this.getProgram(), this.getProgram().getHandler());
	}

	/**
	 * Create a statement that represents a jump of the worm executing the
	 * program
	 */
	public Statement createJump(int line, int column) {
		return new Jump(line, column, this.getProgram(), this.getProgram().getHandler());
	}

	/**
	 * Create a statement that represents toggling the weapon of the worm
	 * executing the program
	 */
	public Statement createToggleWeap(int line, int column) {
		return new ToggleWeap(line, column, this.getProgram(), this.getProgram().getHandler());
	}

	/**
	 * Create a statement that represents firing the current weapon of the worm
	 * executing the program, where the propulsion yield is given by the yield
	 * expression
	 */
	public Statement createFire(int line, int column, Expression yield) {
		return new Shoot(line, column, yield, this.getProgram(), this.getProgram().getHandler());
	}
	
	/**
	 * Create a statement that represents no action of a worm
	 */
	public Statement createSkip(int line, int column) {
		return new Skip(line, column, this.getProgram(), this.getProgram().getHandler());
	}

	/* other statements */

	/**
	 * Create a statement that represents the assignment of the value of the rhs
	 * expression to a variable with the given name
	 */
	public Statement createAssignment(int line, int column, String variableName, Expression rhs) {
		return new Assignment(line, column, variableName, rhs, this.getProgram());
	}

	/**
	 * Create a statement that represents the conditional execution of the
	 * statements then or otherwise, depending on the value of the condition
	 * expression
	 */
	public Statement createIf(int line, int column, Expression condition, Statement then, Statement otherwise) {
		return new If(line, column, condition, then, otherwise, this.getProgram());
	}

	/**
	 * Create a statement that represents the repeated execution of the body
	 * statement, as long as the value of the condition expression evaluates to
	 * true
	 */
	public Statement createWhile(int line, int column, Expression condition, Statement body) {
		return new While(line, column, condition, body, this.getProgram());
	}

	/**
	 * Create a statement that represents the repeated execution of the body
	 * statement, where for each execution the value of the variable with the
	 * given name is set to a different object of the given type.
	 */
	public Statement createForeach(int line, int column, ForeachType type, String variableName, Statement body) {
		return new Foreach(line, column, type, variableName, body, this.getProgram());
	}

	/**
	 * Create a statement that represents the sequential execution of the given
	 * statements
	 */
	public Statement createSequence(int line, int column, List<Statement> statements) {
		return new Sequence(line, column, statements, this.getProgram());
	}

	/**
	 * Create a statement that represents printing out the value of the
	 * expression e
	 */
	public Statement createPrint(int line, int column, Expression e) {
		return new Print(line, column, e, this.getProgram());
	}

	/* types */

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'double'.
	 */
	public Type createDoubleType() {
		return new DoubleType();
	}

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'boolean'.
	 */
	public Type createBooleanType() {
		return new BooleanType();
	}

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'entity'.
	 */
	public Type createEntityType() {
		return new EntityType();
	}

}
