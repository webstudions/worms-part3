package worms.model.programs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import worms.gui.game.IActionHandler;
import worms.model.Worm;
import worms.model.programs.expressions.Expression;
import worms.model.programs.expressions.booleanresult.singular.BooleanConstant;
import worms.model.programs.expressions.entity.NullEntity;
import worms.model.programs.expressions.numeralresult.singular.DoubleConstant;
import worms.model.programs.statements.*;
import worms.model.programs.types.BooleanType;
import worms.model.programs.types.DoubleType;
import worms.model.programs.types.EntityType;
import worms.model.programs.types.Type;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Program {

	public Program(IActionHandler handler) {
		this.handler = handler;
	}
	
	private final IActionHandler handler;
	
	public IActionHandler getHandler() {
		return this.handler;
	}
	
	public void setGlobals(Map<String, Type> globals) {
		this.globals = globals;
		this.initializeVariables();
	}
	
	private void initializeVariables() {
		Iterator<Entry<String, Type>> globals = this.getGlobals().entrySet().iterator();
		while (globals.hasNext()) {
			Entry<String, Type> nextGlobal = globals.next();
			if(nextGlobal.getValue() instanceof DoubleType)
				this.putVar(nextGlobal.getKey(), new DoubleConstant(0, 0, 0.0));
			if(nextGlobal.getValue() instanceof BooleanType)
				this.putVar(nextGlobal.getKey(), new BooleanConstant(0, 0, false));
			if(nextGlobal.getValue() instanceof EntityType)
				this.putVar(nextGlobal.getKey(), new NullEntity(0, 0, this));
		}
	}
	
	public Map<String, Type> getGlobals() {
		return this.globals;
	}
	
	private Map<String, Type> globals;
	
	public void setMainStatement(Statement statement) {
		this.mainStatement = statement;
		this.setPosition(statement.getLine()-1, 0);
	}
	
	public Statement getMainStatement() {
		return this.mainStatement;
	}
	
	private Statement mainStatement;
	
	public void setFactory(Factory factory) {
		this.factory = factory;
	}
	
	public Factory getFactory() {
		return this.factory;
	}
	
	private Factory factory;
	
	public void setWorm(Worm worm) {
		this.worm = worm;
	}
	
	public Worm getWorm() {
		return worm;
	}
	
	private Worm worm;
	
	public void putVar(String name, Expression value) {
		varMap.put(name, value);
	}
	
	public Expression getVar(String name) {
		return (Expression) varMap.get(name);
	}
	
	private HashMap<String, Object> varMap = new HashMap<String, Object>();
	 
	public void execute() {
		try {
			this.paused = false;
			mainStatement.execute();
			if (!this.getPaused()) {
			this.setPosition(this.getMainStatement().getLine()-1, 0);
			this.execute();
			}
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc.getMessage());
		} 
	}
	
	public static boolean isWellFormed(Statement statement) {
		if (statement instanceof Foreach) {
			return (!hasActionStatements(statement));
		} else if (statement instanceof BodyStatements) {
			return Program.isWellFormed( ((BodyStatements)statement).getBody() );
		} else if (statement instanceof If) {
			return (Program.isWellFormed( ((If)statement).getThen() ) && (Program.hasActionStatements( ((If)statement).getOtherwise() ) ) );
		} else if (statement instanceof Sequence) {
			for (Statement seqStat : ((Sequence)statement).getStatements()) {
				Program.isWellFormed(seqStat);
			} 
		} return true;
	}

	private static boolean hasActionStatements(Statement statement) {
		if (statement instanceof ActionStatements) 
			return true;
		else if (statement instanceof BodyStatements) {
			return Program.hasActionStatements( ((BodyStatements)statement).getBody() );
		} else if (statement instanceof If) {
			return (Program.hasActionStatements( ((If)statement).getThen() ) && (Program.hasActionStatements( ((If)statement).getOtherwise() ) ) );
		} else if (statement instanceof Sequence) {
			for (Statement seqStat : ((Sequence)statement).getStatements()) {
				Program.hasActionStatements(seqStat);
			} 
		} 
		return false;
	};
	
	public void setPosition(int line, int col) {
		this.line = line;
		this.col = col;
	}
	
	public void setPause() {
		this.paused = true;
	}
	
	public boolean getPaused() {
		return this.paused;
	}
	
	public int getLine() {
		return this.line;
	}
	
	public int getCol() {
		return this.col;
	}
	
	boolean paused;
	int line = 0;
	int col;
	
};
	