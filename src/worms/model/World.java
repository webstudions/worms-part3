package worms.model;

import java.util.ArrayList;
import java.util.Random;

import be.kuleuven.cs.som.annotate.*;
import worms.model.programs.Program;
import worms.util.Util;

/**
 * @invar	The gameState of this world is always valid.
 * 			| isValidGameState(this.getGamestate())
 * @invar	The world contains maximum 10 teams
 * 			| this.getTeams().size() <= 10
 * @author 	nickscheynen
 * @author 	carmenvandeloo
 *
 */
public class World {
	/**
	 * Creates a new world.
	 * @param 	width 
	 * 			The width of the world (in meter)
	 * @param 	height 
	 * 			The height of the world (in meter)
	 * @param 	passableMap 
	 * 			A rectangular matrix indicating which parts of the terrain are passable and impassable.     
	 * @param 	random 
	 * 			A random number generator.
	 * @post	The width of this World is equal to the given width.
	 * 			| new.getWidth() == width
	 * @post	The height of this World is equal to the given height.
	 * 			| new.getHeight() == height
	 * @post	The passable map of this world is equal to the given passable map.
	 * 			| new.getPassableMap() == passableMap
	 * @post 	The number of pixels per meter in width is equal to the width of the passable map divided by the width of the map.
	 * 			| new.PPMHWidth == this.passableMap[0].length/this.getWidth();
	 * @post 	The number of pixels per meter in height is equal to the height of the passable map divided by the height of the map.
	 * 			| new.PPMHHeight == this.passableMap.length/this.getHeight();
	 * @post 	The number of meter per pixel is equal to the height of the map divided by the height of the passable map.
	 * 			| new.MPP == this.getHeight()/this.passableMap.length;
	 * @post	The Gamestate is set to INITIALISING
	 * 			| new.GameState == INITIALISING;
	 * @throws	IllegalArgumentException
	 * 			| width<0 || Double.isNaN(width) || height < 0 || Double.isNaN(height)
	 */
	@Raw
	public World(double width, double height, boolean[][] passableMap, Random random) throws IllegalArgumentException {
		if(width<0 || Double.isNaN(width) || height < 0 || Double.isNaN(height))
			throw new IllegalArgumentException();
		this.width = width;
		this.height = height;
		this.passableMap = passableMap;
		this.PPMWidth = this.passableMap[0].length/this.getWidth();
		this.PPMHeight = this.passableMap.length/this.getHeight();
		this.MPP = this.getHeight()/this.passableMap.length;
		this.setStepSize();
		this.randomGenerator = random;
		this.gameState = GameState.INITIALISING;
	}
	
	/**
	 * Return the width of this world.
	 */
	public double getWidth() {
		return width;
	}
	
	private final double width;
	
	/**
	 * Return the height of this world.
	 */
	public double getHeight() {
		return height;
	}
	
	private final double height;
	
	/**
	 * Return the passableMap of this world.
	 */
	public boolean[][] getPassableMap() {
		return passableMap;
	}
	
	private final boolean[][] passableMap;
	
	/**
	 * Return the random number generator of this world.
	 */
	public Random getRandomGenerator() {
		return randomGenerator;
	}
	
	private final Random randomGenerator;
	
	/**
	 * Return the number of pixels per meter in height of this world.
	 */
	public final double getPPMHeight() {
		return this.PPMHeight;
	}
	
	private final double PPMHeight;

	/**
	 * Return the number of pixels per meter in width of this world.
	 */
	public final double getPPMWidth() {
		return this.PPMWidth;
	}
	
	private final double PPMWidth;
	
	/**
	 * Returns the number of meter per pixel in this world.
	 */
	protected double getMPP() {
		return this.MPP;
	}
	
	private final double MPP; 
	
	/**
	 * Sets the stepsize to use in this world.
	 * @post	the stepsize of this world is set to the right value depending on the MPP of this world.
	 * 			| if this.getMPP() < 1 then { new.stepsize = this.getMPP() }
	 * 			| if this.getMPP() >=  1 then { new.stepsize = 0.0125 }
	 */
	private void setStepSize() {
		if(this.getMPP() < 1) {
			this.stepSize = this.getMPP();
		} else {
			this.stepSize = 0.0125;
		}
	}
	
	/**
	 * Returns the stepsize for this world.
	 */
	public double getStepSize() {
		return this.stepSize;
	}
	
	private double stepSize;
	
	/**
	 * Create and add an empty team with the given name to the given world.
	 * @param	newName 
	 * 			| name of the team
	 * @post	If a team with the given name doesn't exist already, a new team is added and made the team currently being added. 
	 * 			If it exists no team is added and the team that is being added is set to the team found with this name.
	 * 			| Team teamWithName = getTeamWithName(newName);
	 * 			| if(teamWithName == null) { this.setAddingTeam(newTeam) }
	 * 			| else { this.setAddingTeam( teamWithName ) }
	 * @throws	IllegalArgumentException
	 * 			| catch (IllegalArgumentException)
	 * @throws	UnsupportedOperationException
	 * 			| this.getTeams().size()==10
	 */
	public void addEmptyTeam(String newName) {
		if(this.getTeams().size()==10)
			throw new UnsupportedOperationException();
		if (this.getGameState() == GameState.INITIALISING) {
			Team teamWithName = getTeamWithName(newName);	
			if(teamWithName == null) {
				try {
					Team newTeam = new Team(this, newName);
					this.getTeams().add(newTeam);
					this.setAddingTeam(newTeam);
				} catch(IllegalArgumentException exc) {
					throw new IllegalArgumentException(exc);
				}
			} else {
				this.setAddingTeam( teamWithName );
			}
		}
	};
	
	/**
	 * Returns an ArrayList with all the teams in the given world
	 */
	protected ArrayList<Team> getTeams() {
		return this.worldTeams;
	};
	
	/**
	 * Checks if a team with the given name exists and if so returns the team object.
	 * @param 	name 
	 * 			The name of the team.
	 * @return	Returns the team object if a team with the given name exists and null if it doesn't.
	 * 			| if(worldTeams.get(i).getName().equals(name)) { result == worldTeams.get(i); }
	 */
	private Team getTeamWithName(String name) {
		ArrayList<Team> worldTeams = this.getTeams();
		for (int i = 0; i < worldTeams.size(); i++) {
			if(worldTeams.get(i).getName().equals(name)) {
				return worldTeams.get(i);
			}
		}
		return null;
	}
	
	private ArrayList<Team> worldTeams = new ArrayList<Team>();
	
	/**
	 * Sets the team that is currently being added to the world.
	 * @param 	team 
	 * 			Team object.
	 * @post	The team that is currently being added to the world is the given team.
	 * 			| new.addingTeam == team;
	 */
	private void setAddingTeam(Team team) {
		this.addingTeam = team;
	}
	
	/**
	 * Returns the team that is currently being added to the world.
	 */
	private Team getAddingTeam() {
		return this.addingTeam;
	}
	
	private Team addingTeam = null;

	/**
	 * Create and add a new food ration to a random position of the given world.
	 * @post	A new food ration is created and added to the worldFood list.
	 *			| worldFood.add(newFood);
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */	
	public void addNewFood() throws IllegalArgumentException {
		if (this.getGameState() == GameState.INITIALISING) {
			try {
				Position position = GetValidRandomPosition(Food.getFoodRadius());
				Food newFood = new Food(this,position.getX(),position.getY());
				worldFood.add(newFood);
			} catch(IllegalArgumentException exc) {
				throw new IllegalArgumentException();
			}
		}
	}
	
	/**
	 * Create a food ration that is positioned at the given location in the given world.
	 * @param 	x
	 * 			The x position in meters.
	 * @param 	y
	 * 			The y position in meters.
	 * @param	world
	 * 			The world in which to create the new food ration.
	 * @post	A new food ration is created and added to the worldFood list.
	 *			| worldFood.add(newFood);
	 * @return 	The created food ration.
	 * 			| result == newFood;
	 * @throws 	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */
	public Food addNewCreatedFood(double x, double y) throws IllegalArgumentException {
		try {
			if ( (this.getGameState() == GameState.INITIALISING) ) {
				Food newFood = new Food(this, x, y);
				this.getFood().add(newFood);
				return newFood;
			} else
				return null;
			}
		catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Removes the given food ration from the world.
	 * @param 	food
	 * 			The food object to be removed.
	 * @effect	The food ration is deactivated using the DIE()function
	 * 			| food.DIE()
	 * @post	The food ration is removed from the worldFood arraylist.
	 * 			| new.getFood().contains(food) == false
	 */
	public void removeFood(Food food) {
		food.DIE();
		this.getFood().remove(food);
	}
	
	/**
	 * Returns all the food rations in the world as an ArrayList.
	 */
	public ArrayList<Food> getFood() {
		return this.worldFood;
	};
	
	private ArrayList<Food> worldFood = new ArrayList<Food>();
	
	/**
	 * Create and add a new worm to the given world at a random location, with a direction of 0.2 and a direction of 0.3, as name "Worm"+index and a team if there is an addingTeam.
	 * @effect	The position of the new worm is calculated using the GetValidRandomPosition() method.
	 * 			| Position position = GetValidRandomPosition(0.3);
	 * @effect	The team of the worm is set using the setTeam() method of worm.
	 * 			| newWorm.setTeam(this.getAddingTeam());
	 * @effect	The worm is added to the activeWorldWorms arrayList using the appropriate setter.
	 * 			| this.getActiveWorms().add(newWorm);
	 * @effect	The addingTeam is reset to null using the setAddingTeam() method.
	 * 			| this.setAddingTeam(null);
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */
	public void addNewWorm(Program program) throws IllegalArgumentException {
		if (this.getGameState() == GameState.INITIALISING) {
		try {
			Position position = GetValidRandomPosition(0.35);
			Worm newWorm = new Worm(this,position.getX(),position.getY(), 6.1, 0.35, "Worm "+(this.getActiveWorms().size()+1), program);
			newWorm.setTeam(this.getAddingTeam());
			this.getActiveWorms().add(newWorm);
			this.setAddingTeam(null);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		} }
	};
	
	/**
	 * Creates (adds, and returns) a new worm that is positioned at the given location in this world, looks in the given direction, has the given radius and the given name.
	 * The new worm may (but isn't required to) have joined a team.
	 * @param 	x
	 * 			The x-coordinate of the position of the new worm (in meter)
	 * @param 	y
	 * 			The y-coordinate of the position of the new worm (in meter)
	 * @param 	direction
	 * 			The direction of the new worm (in radians)
	 * @param 	radius 
	 * 			The radius of the new worm (in meter)
	 * @param 	name
	 * 			The name of the new worm
	 * @post	A new worm object is created and added to the worldWorms list.
	 *			| worldWorms.add(newWorm);
	 * @return 	The created worm object.
	 * 			| result == newWorm;
	 * @throws	UnsupportedOperationException
	 * 			| ! this.getGameState() == GameState.INITIALISING
	 * @throws	IllegalArgumentException
	 * 			| catch( IllegalArgumentException )
	 */
	public Worm addNewCreatedWorm(double x, double y, double direction, double radius, String name, Program program) throws UnsupportedOperationException, IllegalArgumentException {
		if (! (this.getGameState() == GameState.INITIALISING) ) 
			throw new UnsupportedOperationException();
		try {
			Worm newWorm = new Worm(this, x, y, direction, radius, name, program);
			newWorm.setTeam(this.getAddingTeam());
			this.getActiveWorms().add(newWorm);
			this.setAddingTeam(null);
			return newWorm;
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
			
	}
	
	/**
	 * Sets the active state of the given worm to false, sets the active projectile to null and removes the worm from the activeWorldWorms arraylist.
	 * @param 	worm
	 * 			Worm object to be removed.
	 * @effect	The worm is deactivated using the DIE() method.
	 * 			| worm.DIE()
	 * @effect	The world's active projectile is set to null using the setActiveProjectile() method
	 * 			| this.setActiveProjectile(null);
	 * @post	The worm is removed from the world's activeWorldWorms arraylist.
	 * 			| !this.getActiveWorms().contains(worm);
	 */
	public void removeWorm(Worm worm) {
		worm.DIE();
		this.setActiveProjectile(null);
		this.getActiveWorms().remove(worm);
		worm.setIsSelected(false);
		if (worm == this.getCurrentWorm())
			if ( this.getIndexNextWorm() >= this.getActiveWorms().size() ) {
				this.setNextWorm();
			}
	}
	
	/**
	 * Returns all the active worms in the given world
	 */
	public ArrayList<Worm> getActiveWorms() {
		return this.activeWorldWorms;
	};
	
	private ArrayList<Worm> activeWorldWorms = new ArrayList<Worm>();
	
	/**
	 * Returns the next worm.
	 */
	public Worm getNextWorm() {
		return nextWorm;
	}
	
	/**
	 * Sets the index of the next worm using the setIndexNextWorm() method and sets the next worm.
	 * @effect	Sets the index of the next worm using the setIndexNextWorm().
	 * 			| this.setIndexNextWorm()
	 * @post	Sets the next worm.
	 * 			| new.getNextWorm() == this.getActiveWorms().get(this.getIndexNextWorm());
	 */
	public void setNextWorm() {
		this.setIndexNextWorm();
		this.nextWorm = this.getActiveWorms().get(this.getIndexNextWorm());
	}
	
	private Worm nextWorm;
	
	/**
	 * Returns the index that the next worm has in the arraylist activeWorldWorms.
	 */
	public int getIndexNextWorm() {
		return indexNextWorm;
	}

	/**
	 * Determines the index of the next worm and stores it into indexNextWorm. 
	 * @post	Determines the index of the next worm and stores it into indexNextWorm.
	 * 			| if ( this.getIndexNextWorm() >= this.getActiveWorms().size()-1 )
	 *			| 	this.indexNextWorm = 0;
	 *			| else 
	 *			| 	this.indexNextWorm = this.getIndexNextWorm() + 1;	
	 */
	public void setIndexNextWorm() {
		if ( this.getIndexNextWorm() >= this.getActiveWorms().size()-1 )
			this.indexNextWorm = 0;
		else 
			this.indexNextWorm = this.getIndexNextWorm() + 1;
	}
	
	private int indexNextWorm;

	/**
	 * Returns the active worm in the given world (i.e., the worm whose turn it is).
	 */
	public Worm getCurrentWorm() {
		return currentWorm;
	}
	
	/**
	 * Sets the currentWorm to the given worm.
	 * @param 	currentWorm 
	 * 			The worm that will be set as the current worm.
	 */
	public void setCurrentWorm(Worm currentWorm) {
		this.currentWorm = currentWorm;
	}
	
	private Worm currentWorm;
	
	/**
	 * Returns an arraylist containing all the active worms and food rations of this world.
	 */
	public ArrayList<GameObject> getGameObjects() {
		ArrayList<GameObject> list = new ArrayList<GameObject>();
		list.addAll(this.getActiveWorms());
		list.addAll(this.getFood());
		return list;
	}

	/**
	 * Sets the world's gamestate to the given gamestate
	 * @param 	gamestate
	 * 			The gamestate to be set.
	 * @post	The world's gamestate is set to the given gamestate
	 * 			| new.getGameState() == gamestate
	 * @throws 	IllegalArgumentException
	 * 			| !isValidGameState(gamestate)
	 */
	@Raw
	private void setGameState(GameState gamestate) throws IllegalArgumentException {
		if(!isValidGameState(gamestate))
			throw new IllegalArgumentException();
		this.gameState = gamestate;
	}
	
	/**
	 * Gets the gamestate of this world.
	 */
	public GameState getGameState() {
		return this.gameState;
	}
	
	/**
	 * Checks if the given gamestate is a valid gamestate.
	 * @param 	gamestate
	 * 			The given gamestate.
	 * @return 	Returns true if the gamestate is valid.
	 * 			| return (gameState == GameState.INITIALISING) || (gameState == GameState.PLAYING);
	 */
	private boolean isValidGameState(GameState gamestate) {
		return (gameState == GameState.INITIALISING) || (gameState == GameState.PLAYING);
	}
	
	private GameState gameState;
	
	/**
	 * Returns a valid random position given the radius of the radius of the GameObject.
	 * @param 	radius
	 * 			The radius of the GameObject for which we need a valid random position.
	 * @return	Returns a valid random position given the radius of the radius of the GameObject.
	 * 			| while (! isOutsideWorld(validPosition) || validPosition == null ) {
	 * 			|	randomPerimeterPosition == getRandomPerimeterPosition();
	 * 			| 	validPosition = PerimeterPositionToValidPosition( randomPerimeterPosition , radius);
	 * 			|  	result == validPosition
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(radius)
	 */
	private Position GetValidRandomPosition(double radius) throws IllegalArgumentException {
		if(Double.isNaN(radius)) 
			throw new IllegalArgumentException();
		Position validPosition = new Position(-1.0, -1.0);
		while( (validPosition == null) || (this.isOutOfWorld(validPosition.getX(), validPosition.getY(), radius)) ) {
			Position randomPerimeterPosition =  this.getRandomPerimeterPosition();
			validPosition = PerimeterPositionToValidPosition( randomPerimeterPosition , radius);
		}
		return validPosition;
		
	}
	
	/**
	 * Returns a random position on the perimeter of the world.
	 * @return	Returns a random position on the perimeter of the world.
	 * 			| if( perimeterLocation < height ) 
	 * 			|	then {result = Position(this.getStepSize(), perimeterLocation) }
	 * 			| if( (perimeterLocation >= height) && (perimeterLocation < height+width) ) 
	 * 			|	then {result = Position(perimeterLocation-height, (height-this.getStepSize()) )}
	 * 			| if( (perimeterLocation >= height+width) && (perimeterLocation < height*2 + width)) 
	 * 			|	then { result = Position( (width - this.getStepSize()) , (height - (perimeterLocation-height-width)) )}
	 * 			| else
	 * 			| 	{ result = Position(width - (perimeterLocation-2*height-width), this.getStepSize())}
	 */
	private Position getRandomPerimeterPosition() {
		double height = this.getHeight();
		double width = this.getWidth();
		double maxLength = 2 * width + 2 * height;
		double perimeterLocation = maxLength * this.randomGenerator.nextDouble();
		
		if(perimeterLocation < height) {
			return new Position(this.getStepSize(), perimeterLocation); 
		} else if( (perimeterLocation >= height) && (perimeterLocation < height+width) ) {
			return new Position(perimeterLocation-height, (height-this.getStepSize()) );
		} else if( (perimeterLocation >= height+width) && (perimeterLocation < height*2 + width)) {
			return new Position( (width - this.getStepSize()) , (height - (perimeterLocation-height-width)) );
		} else {
			return new Position(width - (perimeterLocation-2*height-width), this.getStepSize());
		}
	}
	
	/**
	 * Converts to given perimetPosition
	 * @param 	pPosition
	 * 			Position on the perimeter of the world.
	 * @param 	radius
	 * 			Radius of the GameObject to be placed in the world.
	 * @return	The method checks where the perimeter position is compared to the center of the map and then checks for a valid position in the direction of the center, returning the first one found.
	 *			| result == calculateFirstValidPosition(pPosition, radius, DirectionToCenter)
	 */
	private Position PerimeterPositionToValidPosition(Position pPosition, double radius) {
		if( (pPosition.getX()==this.getStepSize()) && pPosition.getY()<this.getHeight()) {
			// go right
			return calculateFirstValidPosition(pPosition, radius, Direction.RIGHT);
		} else if( (pPosition.getX()==this.getWidth()-this.getStepSize()) && (pPosition.getY()<this.getHeight()) ) {
			// go left
			return calculateFirstValidPosition(pPosition, radius, Direction.LEFT);
		} else if( (pPosition.getY()==this.getStepSize()) && (pPosition.getX()<this.getWidth()) ) {
			// go up
			return calculateFirstValidPosition(pPosition, radius, Direction.UP);
		} else if ( (pPosition.getY()==this.getHeight()-this.getStepSize()) && (pPosition.getX()<this.getHeight()) ) {
			// go down
			return calculateFirstValidPosition(pPosition, radius, Direction.DOWN);
		} else {
			return null;
		}
	}
	
	/**
	 * Enumeration of all existing directions (Right, Left, Down, Up)
	 */
	private enum Direction {
		RIGHT,
		LEFT,
		UP,
		DOWN
	}
	
	/**
	 * Calculates the first valid position for a GameObject with the given radius, pPosition and direction to the center.
	 * @param 	pPosition
	 * 			The randomly generated perimeter position of this gameobject from where to start searching for a valid position.
	 * @param 	radius
	 * 			The radius of the gameObject to find a valid position for.
	 * @param 	dir
	 * 			The direction from the pPosition to the center of this world.
	 * @return	Returns the first valid position for a GameObject with the given radius, pPosition and direction to the center.
	 * 			| result == tryPosition
	 */
	private Position calculateFirstValidPosition(Position pPosition, double radius, Direction dir) {
		Position tryposition = pPosition;
		
		while( !(this.isAdjacent(tryposition.getX(), tryposition.getY(), radius)) && !(isOutOfWorld(tryposition)) ) {
			
			if(dir == Direction.RIGHT) {
				tryposition = new Position( (tryposition.getX() + (this.getStepSize() * radius)) , tryposition.getY());
			}
				
			if(dir == Direction.LEFT)
				tryposition = new Position( (tryposition.getX() - (this.getStepSize() * radius)) , tryposition.getY());
			if(dir == Direction.UP) {
				tryposition = new Position( tryposition.getX(), (tryposition.getY() + (this.getStepSize() * radius)) );
			}
				
			if(dir == Direction.DOWN)
				tryposition = new Position( tryposition.getX() , (tryposition.getY() - (this.getStepSize() * radius)) );

		}
		return tryposition;
	}
	
	/**
	 * Returns true if the given position lies outside the boundaries of the world.
	 * @param 	position
	 * 			The position to check.
	 * @return	Returns true if the given position lies outside the boundaries of the world.
	 * 			| result == position.getX() < 0.0 || position.getX() > this.getWidth() || position.getY() < 0.0 || position.getY() > this.getHeight();
	 */
	public boolean isOutOfWorld(Position position) {
		return position.getX() < 0.0 || position.getX() > this.getWidth() 
			|| position.getY() < 0.0 || position.getY() > this.getHeight(); 
	}
	
	/**
	 * Returns whether a game object with a given radius is outside this world or not.
	 * @param	x
	 * 			the x coordinate of the GameObject to check.
	 * @param	y
	 * 			the y coordinate of the GameObject to check.
	 * @param	radius
	 * 			the radius of the GameObject to check.
	 * @return	Returns whether the game object is outside this world or not.
	 * 			| result = ((x + radius > this.getWidth()) || (x - radius < 0) || (y + radius > this.getHeight()) || (y - radius < 0)) )
	 */
	public boolean isOutOfWorld(double x, double y, double radius) {
		return ( (x + radius > this.getWidth()) || 
				 (x - radius < 0) || 
				 (y + radius > this.getHeight()) ||
				 (y - radius < 0));
	}
	
	/**
	 * Checks whether the given circular region of this world, defined by the given center coordinates and radius, is passable and adjacent to impassable terrain. 
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return	False if the given region is (at least partially) outside the world.
	 * 			| result = !this.isOutofWorld(x, y, radius)
	 * @return 	True if the given region is passable and adjacent to impassable terrain, false otherwise.
	 * 			| result = ( (!isImpassable(x, y, radius) && closeToImpassable(x, y, radius)) )
	 */
	public boolean isAdjacent(double x, double y, double radius) {
		if(this.isOutOfWorld(x, y, radius))
			return false;
		else
			return (!isImpassable(x, y, radius) && closeToImpassable(x, y, radius));
	};
	
	/**
	 * Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * The method will make sure that if the object's radius only 1px, it will check radius + this.getStepSize().  
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return	The method will make sure that if the object's radius only 1px, it will check radius + this.getStepSize(). 	
	 * 			Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * 			| if( border<intRadius + 1 ) {
	 *			|	result = isImpassable(x, y, radius + this.getStepSize())
	 *			|  } 
	 * @return 	Returns true if the given circular region of this world, defined by the given center coordinates and radius, is adjacent to impassable terrain.
	 * 			| result = isImpassable(x, y, 1.1 * radius);
	 */
	public boolean closeToImpassable(double x, double y, double radius) {
		int intRadius = doubleToInt( Math.ceil( meterToPixel(radius) ) );
		if (intRadius == Integer.MAX_VALUE) return false;
		
		int border = doubleToInt( Math.ceil( meterToPixel(radius * 1.1) ) );
		// This checks if the closeToImpassable will check more pixels than those of the gameObject.  
		// Necessary when the circular area of the GameOjbect is smaller than a pixel.
		if(border < intRadius + 1) {
			return isImpassable(x, y, radius + this.getStepSize());
		} else  {
			return isImpassable(x, y, 1.1 * radius);
		}
		
	}
	
	/**
	 * Checks whether the given circular region of the given world, defined by the given center coordinates and radius, is impassable. 
	 * @param 	world 
	 * 			The world in which to check impassability 
	 * @param 	x 
	 * 			The x-coordinate of the center of the circle to check  
	 * @param 	y 
	 * 			The y-coordinate of the center of the circle to check
	 * @param 	radius 
	 * 			The radius of the circle to check
	 * @return	Returns whether the given circular region of the given world, defined by the given center coordinates and radius, is impassable.
	 * 			| for(int i = leftUpX; i<=rightDownX; i++) {
	 * 			| 	for(int j = leftUpY; j<=rightDownY; j++) {
	 * 			| 		if( Math.sqrt( Math.pow((i - intX), 2) + Math.pow((j - intY), 2) ) <= (intRadius+1) ) {
	 * 			| 			if(passableMap[j][i] == false) 
	 * 			| 				result = true;
	 * 			| else result = false;
	 */
	
	public boolean isImpassable(double x, double y, double radius) {
		
		if( !Double.isNaN(x) && !Double.isNaN(y) && !Double.isNaN(radius) ) {
			int intRadius = doubleToInt( Math.ceil( meterToPixel(radius) ) );
			
			// Calculate the x coordinate of the left top pixel of the square around the center of the circle.
			int intX = doubleToInt( Math.floor(meterToPixel(x)) );
			int leftUpX;
			leftUpX = doubleToInt( Math.floor(meterToPixel(x-radius)) );
			if (leftUpX == Integer.MAX_VALUE) return false;
			
			// Calculate the y coordinate of the left top pixel of the square around the center of the circle.
			int leftUpY = doubleToInt( meterToPixel(this.getHeight()) ) - doubleToInt( Math.ceil(meterToPixel(y+radius)) );
			if (leftUpY == Integer.MAX_VALUE) return false;
			
			// Calculate the x coordinate of the right down pixel of the square around the center of the circle.
			int rightDownX;
			if( Util.fuzzyEquals( Math.round(meterToPixel(x+radius)) , meterToPixel(x+radius) ) )
				rightDownX = doubleToInt( (meterToPixel(Math.round(x+radius))) ) - 1;
			else 
				rightDownX = doubleToInt( Math.floor(meterToPixel(x+radius)) );
			if (rightDownX == Integer.MAX_VALUE) return false;
			
			// Calculate the y coordinate of the left top pixel of the square around the center of the circle.
			int intY = doubleToInt( meterToPixel(this.getHeight()) ) - doubleToInt( Math.ceil(meterToPixel(y)) );
			int rightDownY;
			if( Util.fuzzyEquals( Math.round( meterToPixel(y-radius)) , meterToPixel(y-radius) ) )
				rightDownY = doubleToInt( meterToPixel(this.getHeight()) ) - doubleToInt( Math.ceil(meterToPixel(Math.round(y-radius))) ) - 1;
			else 
				rightDownY = doubleToInt( meterToPixel(this.getHeight()) ) - doubleToInt( Math.ceil(meterToPixel(y-radius)) );
			if (rightDownY == Integer.MAX_VALUE) return false;
			
			// Make sure no arrayOutOfBounds exception will occur.
			if(leftUpX<0)
				leftUpX = 0;
			if(leftUpY<0)
				leftUpY = 0;
			if(rightDownX> passableMap[0].length-1)
				rightDownX = passableMap[0].length-1;
			if(rightDownY> passableMap.length-1)
				rightDownY = passableMap.length-1;
			
			// Check the pixels inside the circle around the center
			for(int i = leftUpX; i<=rightDownX; i++) {
				for(int j = leftUpY; j<=rightDownY; j++) {
					if( Math.sqrt( Math.pow((i - intX), 2) + Math.pow((j - intY), 2) ) <= (intRadius+1) ) {
						if(passableMap[j][i] == false) 
							return true;
					}
				}
			}
			return false;
	
		} else {
			return false;
		}
	}
	
	/**
	 * Converts meters to pixels.
	 * @param 	meter
	 * 			The amount of meter to be converted.
	 * @return	Returns the given meters in pixels.
	 * 			| result = meter * this.getPPMWidth()
	 */
	private double meterToPixel(double meter) {
		return meter * this.getPPMWidth();
	}
	
	/**
	 * Converts a double into an integer. Returning Integer.MAX_VALUE if the converted integer would be outside the integer range.
	 * @param 	d
	 * 			The double to convert to an integer.
	 * @return	Returns a double as an integer. Returning Integer.MAX_VALUE if the converted integer would be outside the integer range.
	 * 			| if ( Math.round(d) >= Integer.MAX_VALUE) then (result = Integer.MAX_VALUE)
	 * 			| else result = (int) Math.round(d)
	 */
	private int doubleToInt(double d) {
		long l = Math.round(d);
		if (l >= Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		} else {
			return (int) l;
		}
	}
	
	/**
	 * Starts a game in the given world.
	 * @effect	Sets the current and next worm using the appropriate setters.
	 * 			| this.setCurrentWorm(firstWormOfThisWorld);
	 * 			| this.setNextWorm();
	 * @effect	Sets the gameState to PLAYING using the setGameState() method.
	 * 			| this.setGameState(GameState.PLAYING)
	 * @effect	Sets the isSelected value of the first worm in this world to true using the setIsSelected() method.
	 * 			| firstWormOfThisWorld.setIsSelected(true)
	 * @effect	Activates the first weapon of the first worm of this world using the activate() method.
	 * 			| firstWormOfThisWorld.firstWeapon.activate()
	 * @effect	If the first worm of this world is a computer worm, the worm will execute it's program using the execute() method.
	 * 			| if ( firstWormOfThisWorld.getHasProgram == true ) then ( firstWormOfThisWorld.getProgram().execute }
	 * @throws	IllegalArgumentException()
	 * 			| catch (IllegalArgumentException)	
	 */
	public void startGame(){
		if (this.getNbOfWorms() != 0) {
			Worm firstWormOfThisWorld = this.getActiveWorms().get(0);
			this.setCurrentWorm(firstWormOfThisWorld);
			this.setNextWorm();
			this.setGameState(GameState.PLAYING);
			firstWormOfThisWorld.setIsSelected(true);
			firstWormOfThisWorld.getWormProjectiles().get(0).activate();
			if(firstWormOfThisWorld.getHasProgram() == true ) {
				try {
					firstWormOfThisWorld.getProgram().execute();
				} catch(IllegalArgumentException exc) {
					throw new IllegalArgumentException(exc.getMessage());
				}
			}
		}
	}
	
	/**
	 * Returns the number of worms in this world.
	 * @return	Returns the number of worms in this world.
	 * 			| result = this.getActiveWorms().size()
	 */
	private int getNbOfWorms () {
		return this.getActiveWorms().size();
	}
	
	/**
	 * Updates all the variables that need to be changed when switching the turn.
	 * @effect	The selected weapon of the current worm is deactivated using the deactivate method.
	 * 			| this.getCurrentWorm().getSelectedWeapon().deactivate();
	 * @effect	The current worm is deselected using the setIsSelected() method.
	 * 			| this.getCurrentWorm().setIsSelected(false);
	 * @effect	Sets the next worm as the new current worm.
	 * 			| this.setCurrentWorm(nextWorm);
	 * @effect 	Sets the new next worm.
	 * 			| this.setNextWorm();
	 * @effect	The new current worm's action points are set to the maximum action points using the setActionPoints() method.
	 * 			| new.getCurrentWorm().setActionPoints(new.getCurrentWorm().getMaximumActionPoints());
	 * @effect	The new current worm's hitpoints are increased with 10, using the setHitPoints() method;
	 * 			| new.getCurrentWorm().setHitPoints(new.getCurrentWorm().getHitPoints()+10);
	 * @effect 	The new current worm's state is set to selected, using the setIsSelected() method.
	 * 			| new.getCurrentWorm().setIsSelected(true);
	 * @effect	The new current worm's first projectile is activated using the activate() method.
	 * 			| new.getCurrentWorm().getWormProjectiles().get(0).activate();
	 */
	public void switchTurn() {
		this.getCurrentWorm().getSelectedWeapon().deactivate();
		this.getCurrentWorm().setIsSelected(false);
			
		this.setCurrentWorm(nextWorm);
		this.setNextWorm();
			
		this.getCurrentWorm().setActionPoints(this.getNextWorm().getMaximumActionPoints());
		this.getCurrentWorm().setHitPoints(this.getNextWorm().getHitPoints()+10);
		this.getCurrentWorm().setIsSelected(true);
		this.getCurrentWorm().getWormProjectiles().get(0).activate();
	};
	
	/**
	 * Starts the next turn in the given world.
	 * @effect	Updates the variables to set the new turn, using the switchTurn() method.
	 * 			| this.switchTurn();
	 * @effect	If the next worm of this world is a computer worm, the worm will execute it's program using the execute() method.
	 * 			| if ( nextWorm.getHasProgram == true ) then ( nextWorm.getProgram().execute }	
	 * @throws	IllegalArgumentException
	 * 			| catch (IllegalArgumentException) 
	 * */
	public void startNextTurn() throws IllegalArgumentException {
		this.switchTurn();
		if(this.getCurrentWorm().getHasProgram() == true ) {
			try {
				this.getCurrentWorm().getProgram().execute();
			} catch(IllegalArgumentException exc) {
				throw new IllegalArgumentException(exc.getMessage());
			}
		}
	};
	
	/**
	 * Returns the name of a single worm if that worm is the winner, or the name of a team if that team is the winner. This method should null if there is no winner.
	 * @return	Returns the name of a single worm if that worm is the winner, or the name of a team if that team is the winner. This method returns null if there is no winner.
	 * 			| if (activeWorms.size() == 1)
	 * 			|	 { result = ( activeWorms.get(0).getName() || activeWorms.get(0).getTeamName()) }
	 * 			| else {
	 * 			|	if(nbParties==1) {result = winnableTeamList.get(0).getName()} 
	 * 			| else {result = null}
	 */
	public String getWinner() {
		if (this.getGameState() == GameState.PLAYING) {
			ArrayList<Worm> activeWorms = this.getActiveWorms();
			//no active worms.
			if (activeWorms.size() == 0) {
				return null;
			}
			//only one active worm.
			if (activeWorms.size() == 1) {
				if ( activeWorms.get(0).getTeamName() == null ) {
					return activeWorms.get(0).getName();
				}
				else return activeWorms.get(0).getTeamName();
			}
			//multiple active worms.
			else {
				int nbParties = 0;
				ArrayList<Team> winnableTeamList = new ArrayList<Team>();
				for(int i = 0; i<activeWorms.size();i++) {
					if(activeWorms.get(i).getTeam() == null) {
						nbParties=nbParties+1;
					} else {
						if(!winnableTeamList.contains(activeWorms.get(i).getTeam())) {
							winnableTeamList.add(activeWorms.get(i).getTeam());
							nbParties= nbParties+1;
						}
					}
				}
				if(nbParties==1) {
					return winnableTeamList.get(0).getName();
				} else {
					return null;
				}
			}
		}
		//if gamestate is not PLAYING.
		return null;
	};
	
	
	/**
	 * Returns whether the game in the given world has finished.
	 * @return	Returns whether the game in the given world has finished.
	 * 			| if (this.getWinner() == null) { 
	 * 			|	if(this.getActiveWorms().size()==0) { result = true; } else { result = false; }
	 * 			| } else {result = true}
	 */
	public boolean isGameFinished() {
		if (this.getWinner() == null) {
			if(this.getActiveWorms().size()==0) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	};
	
	/**
	 * Makes the given projectile the active projectile of this world.
	 * @param 	projectile
	 * 			The projectile to make the active projectile of this world.
	 * @post	news.getActiveProjectile() == projectile
	 */
	public void setActiveProjectile(Projectile projectile) {
		this.activeProjectile = projectile;
	}
	
	/**
	 * Returns the active projectile in the world, or null if no active projectile exists.
	 */
	public Projectile getActiveProjectile() {
		return this.activeProjectile;
	};
	
	private Projectile activeProjectile;
	
};