package worms.model;

import java.util.ArrayList;

import worms.model.programs.Program;
import worms.util.Util;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * @invar	The radius of this game object is a number at all times bigger than the minimum radius and smaller than positive infinity.
 * 			| isValidRadius(this.getRadius());
 * @invar	The action points of this worm are at all times bigger than 0 (or equal to) and smaller than (or equal to) the maximum action points.
 * 			| (this.getActionPoints() >= 0) && (this.getActionPoints <= this.getMaximumActionPoints())
 * @invar	The hit points of this worm are at all times bigger than 0 (or equal to) and smaller than (or equal to) the maximum hit points.
 * 			| (this.getHitPoints() >= 0) && (this.getHitPoints <= this.getMaximumHP())
 * @invar	The name of this worm at all times matches the regular expression: "[A-Z]{1}[a-zA-Z0-9\" \"[\"][\']]+"
 * 			| isValidName(this.getName())
 * @author nickscheynen
 * @author carmenvandeloo
 *
 */
public class Worm extends Jumpable {
	
	/**
	 * Create a new worm that is positioned at the given location in the given world,
	 * looks in the given direction, has the given radius and the given name.
	 * 
	 * @param 	world
	 * 			The world in which to place the created worm  
	 * @param 	x
	 * 		  	The x coordinate for this worm in meters.
	 * @param 	y
	 * 		  	The y coordinate for this worm in meters.
	 * @param 	direction
	 * 		  	The direction this worm is facing in radians.
	 * @param 	radius
	 * 		  	The radius of this worm in meters.
	 * @param 	name
	 * 		  	The name of this worm.
	 * @effect	This worm is initialized as a jumpable object with world as world, x as x, y as y, radius as radius and direction as direction.
	 * 			| super(world, x, y, radius, direction);
	 * @effect	The radius of this worm is equal to the given radius. The mass and number of maximum action and hit points are set accordingly.
	 * 			| this.setSizeAspects(radius)
	 * @effect	The name of this worm is equal to the given name.
	 * 			| this.setName(name)
	 * @effect	The mass of the worm is equal to the mass, as calculated in the setMass() method, expressed in kilograms.
	 * @effect	The maximum number of action points is equal to the maximum number of action points, as calculated in the setMaximumActionPoints method.
	 * @effect	The maximum number of hit points is equal to the maximum number of hit points, as calculated in the setMaximumHP method.
	 * @effect	The number of action points of this worm is equal to the maximum number of action points
	 * 			| this.setActionPoints(this.getMaximumActionPoints())
	 * @effect	The number of hit points of this worm is equal to the maximum number of hit points
	 * 			| this.setHitPoints(this.getMaximumHP())
	 * @effect	Sets the selected state of this worm to selected using the setIsSelected() method.
	 * 			| this.setIsSelected(false);
	 * @effect	Creates a new Rifle and adds it to wormProjectiles using the addProjectile() method.
	 * 			| this.addProjectile(newRifle);
	 * @effect	Creates a new Bazooka and adds it to wormProjectiles using the addProjectile() method.
	 * 			| this.addProjectile(newBazooka);
	 * @throws	IllegalArgumentException
	 * 			| catch (IllegalArgumentException)
	 */
	@Raw
	public Worm (World world, double x, double y, double direction, double radius, String name, Program program) throws IllegalArgumentException {
		super(world, x, y, radius, direction);
		this.setSizeAspects(this.getRadius());
		this.setName(name);
		this.setActionPoints(this.getMaximumActionPoints());
		this.setHitPoints(this.getMaximumHP());
		this.setIsSelected(false);
		if( (program != null) && (program instanceof Program) ) {
			this.setProgram(program);
			this.getProgram().setWorm(this);
			this.setHasProgram(true);
		} else {
			this.setHasProgram(false);
		}
		try {
			Rifle newRifle = new Rifle(this.getWorld(), this.getX(),this.getY(),this.getDirection());
			newRifle.setPosition(this.calculateProjectilePosition(newRifle));
			this.addProjectile(newRifle);
			
			Bazooka newBazooka = new Bazooka(this.getWorld(), this.getX(),this.getY(),this.getDirection());
			newBazooka.setPosition(this.calculateProjectilePosition(newBazooka));
			this.addProjectile(newBazooka);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
		this.setPrevDirection(-1000);
		
	};
	
	/**
	 * Sets all the variables affected by the size of the worm.
	 * @effect	Updates the radius of this worm, using the setRadius() method.
	 * 			| this.setRadius(radius)
	 * @effect	Updates the mass of this worm, using the setMass() method.
	 * 			| this.setMass()
	 * @effect	Updates the maximum action points, using the setMaximumActionPoints() method.
	 * 			| this.setMaximumActionPoints()
	 * @effect	Updates the maximum hit points, using the setMaximumHP() method.
	 * 			| this.setMaximumHP()
	 * @effect 	Limits the action points using the setActionPoints() method if the action points exceed their maximum.
	 * 			| if(this.getActionPoints()>this.getMaximumActionPoints()) this.setActionPoints(this.getMaximumActionPoints());
	 * @effect 	Limits the hit points using the setHitPoints() method if the hit points exceed their maximum.
	 * 			| if(this.getHitPoints()>this.getMaximumHP()) this.setHitPoints(this.getMaximumHP());
	 * @param 	radius
	 * 			The radius of this worm in meters.
	 * @throws 	IllegalArgumentException
	 * 			| ! isValidRadius(radius)
	 */
	@Raw
	public void setSizeAspects(double radius) throws IllegalArgumentException {
		if( !isValidRadius(radius) ) 
			throw new IllegalArgumentException();
		this.setRadius(radius);
		this.setMass();
		this.setMaximumActionPoints();
		if(this.getActionPoints()>this.getMaximumActionPoints()) {
			this.setActionPoints(this.getMaximumActionPoints());
		}
		this.setMaximumHP();
		if(this.getHitPoints()>this.getMaximumHP()) {
			this.setHitPoints(this.getMaximumHP());
		}
	}
	
	/**
	 * Check if the radius is a number bigger than or equal to the minimum radius and smaller than positive infinity.
	 * @param radius
	 * @return	Return true if the radius is bigger than or equal to the minimum radius.
	 * 			| result == Util.fuzzyGreaterThanOrEqualTo(radius, minimumRadius) && !Double.isNaN(radius) && (radius < Double.POSITIVE_INFINITY)
	 */
	@Override 
	public boolean isValidRadius(double radius) {
		return Util.fuzzyGreaterThanOrEqualTo(radius, this.getMinimumRadius()) && !Double.isNaN(radius) && (radius < Double.POSITIVE_INFINITY);
	}
	
	/**
	 * Return the minimum allowed radius for this worm.
	 */
	@Basic @Immutable
	public double getMinimumRadius() {
		return this.minimumRadius;
	}
	
	private final double minimumRadius = 0.25;
	
	/**
	 * Sets the mass of this worm to the given mass.
	 * @post	Sets the mass of this worm using the radius of this worm, expressed in kilograms.
	 * 			| new.getMass() == density * ( 4/3 * Math.PI * Math.pow(this.getRadius(), 3) );
	 */
	@Raw @Override
	protected void setMass() {
		this.mass = density * ( (4.0/3.0) * Math.PI * Math.pow(this.getRadius(), 3) );
	}
	
	private final double density = 1062;

	/**
	 * Return the maximum action points this worm can have.  
	 */
	@Basic
	public int getMaximumActionPoints() {
		return maximumActionPoints;
	}
	
	/**
	 * Sets the maximum action points this worm can have based on its mass.
	 * @post	Sets the maximum action points of this worm using its mass. If the mass leads to a too large integer, max action points are set to max integer.
	 * 			| long newMAP = Math.round( this.getMass() );
				| if (newMAP >= Integer.MAX_VALUE) {
				| 	this.maximumActionPoints = Integer.MAX_VALUE;
				| } else {
				|	this.maximumActionPoints = (int) newMAP; 
				| }
	 */
	@Raw
	private void setMaximumActionPoints() {
		long newMAP = Math.round( this.getMass() );
		if (newMAP >= Integer.MAX_VALUE) {
			this.maximumActionPoints = Integer.MAX_VALUE;
		} else {
			this.maximumActionPoints = (int) newMAP; 
		}
	}

	private int maximumActionPoints;
	
	/**
	 * Return the number of action points of this worm.
	 */
	@Basic
	public int getActionPoints() {
		return actionPoints;
	}
	
	/**
	 * Set the action points of this worm to the given number of action points.
	 * @post	Set the action points of this worm to the given number of action points.
	 * 			| if(actionPoints <= 0) { 
	 * 			|		new.getActionPoints() == 0;
	 * 			| 		this.getWorld().startNextTurn();
	 * 			|	}
	 * 			| else if( (actionPoints > 0) && (actionPoints<=this.getMaximumActionPoints() ) 
	 * 			|	{
	 * 			|		this.actionPoints = actionPoints;
	 * 			|	}
	 * 			| else {
	 * 			|		this.actionPoints = this.getMaximumActionPoints();	
	 * 			|	}
	 * @param 	actionPoints
	 * 			The action points to which we want to set the action points
	 */
	@Raw
	public void setActionPoints(int actionPoints) {
		if(actionPoints <= 0) {
			this.actionPoints = 0;
		} else if( (actionPoints>0) && (actionPoints<=this.getMaximumActionPoints()) ) {
			this.actionPoints = actionPoints;
		} else { // bigger than the maximum
			this.actionPoints = this.getMaximumActionPoints();
		}
	}
	
	private int actionPoints;
	
	/**
	 * Return the maximum hit points this worm can have.
	 */
	@Basic
	public int getMaximumHP() {
		return maximumHP;
	}
	
	/**
	 * Sets the maximum hit points this worm can have based on its mass.
	 * @post	Sets the maximum hit points of this worm using its mass. If the mass leads to a too large integer, max hit points are set to max integer.
	 * 			| long newMHP = Math.round( this.getMass() );
	 *			| if (newMHP >= Integer.MAX_VALUE) {
	 *			| 	this.maximumHP = Integer.MAX_VALUE;
	 *			| } else {
	 *			|	this.maximumHP = (int) newMHP; 
	 *			| }
	 */
	@Raw
	private void setMaximumHP() {
		long newMHP = Math.round( this.getMass() );
		if (newMHP >= Integer.MAX_VALUE) {
			this.maximumHP = Integer.MAX_VALUE;
		} else {
			this.maximumHP = (int) newMHP; 
		}
	}

	private int maximumHP;
	
	/**
	 * Return the number of action points of this worm.
	 */
	@Basic
	public int getHitPoints() {
		return hitPoints;
	}
	
	/**
	 * Set the hit points of this worm to the given number of hit points.
	 * @post	Set the hit points of this worm to the given number of hit points.
	 * 			| if(hitPoints <= 0) { 
	 * 			|		new.getHitPoints() == 0;
	 * 			|		this.getWorld().removeWorm(this);
	 * 			|		this.getWorld().startNextTurn();
	 * 			|	}
	 * 			| else if( (hitPoints > 0) && (hitPoints<=this.getMaximumHP() ) 
	 * 			|	{
	 * 			|		this.hitPoints = hitPoints;
	 * 			|	}
	 * 			| else {
	 * 			|		this.hitPoints = this.getMaximumHP();	
	 * 			|	}
	 * @param 	hitPoints
	 * 			The hit points to which we want to set the hit points
	 */
	@Raw
	public void setHitPoints(int hitPoints) {
		if(hitPoints <= 0) {
			this.hitPoints = 0;
			this.getWorld().removeWorm(this);
		} else if( (hitPoints>0) && (hitPoints<=this.getMaximumHP()) ) {
			this.hitPoints = hitPoints;
		} else { // bigger than the maximum
			this.hitPoints = this.getMaximumHP();
		}
	}
	
	private int hitPoints;
	
	/**
	 * Return the name of this worm.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of this worm to the given name.
	 * @post	Sets the name of this worm to the given name.
	 * 			| new.getName() == name
	 * @param 	name
	 * 			The name that we want to call the worm.
	 * @throws	IllegalArgumentException
	 * 			| ! isValidName(name)
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if(! isValidName(name))
			throw new IllegalArgumentException();
			this.name = name;
	}
	
	/**
	 * Check if the name is valid.
	 * @param 	name
	 * 			The name to check.
	 * @return	Return true if the name matches the regular expression. 
	 * 			Names must start with a capital letter, be at least 2 characters long and 
	 * 			can only use letters (upper case and lower case), quotes (single and double) and spaces.
	 * 			| result == name.matches("[A-Z]{1}[a-zA-Z0-9\" \"[\"][\']]+");
	 */
	public boolean isValidName(String name) {
		return name.matches("[A-Z]{1}[a-zA-Z0-9\" \"[\"][\']]+");
	}
	
	private String name;
	
	/**
	 * Returns whether this worm is the currently selected Worm.
	 */
	public boolean getIsSelected() {
		return this.selected;
	}
	
	/**
	 * Sets the selected state of this worm to true or false. 
	 * @param 	selected
	 * 			Boolean that is true when the worm is the selected worm in a game.
	 */
	public void setIsSelected(boolean selected) {
		this.selected = selected;
	}
	
	private boolean selected;
	
	/**
	 * Sets the program of this worm to the given program.
	 * @param 	program
	 * 			Program this worm should execute.
	 */
	public void setProgram(Program program) {
		this.program = program;
	}
	
	/**
	 * Returns the program object of this worm.
	 */
	public Program getProgram() {
		return this.program;
	}
	
	private Program program;
	
	/**
	 * Sets the hasProgram boolean of this worm.
	 */
	public void setHasProgram(boolean bool) {
		this.hasProgram = bool;
	}
	
	/**
	 * Returns true if this worm has a program.
	 */
	public boolean getHasProgram() {
		return this.hasProgram;
	}
	
	private boolean hasProgram;
	
	/**
	 * If the worm is positioned in passable terrain, the method makes the given worm fall down until it rests on impassable terrain again, adjust the position of the worm's projectiles, decreases it's hitpoints, removes it from the world if necessary and let's it eat if possible.
	 * @effect	Decreases the Y coordinate until the worm is at a valid position or outside the world using the setY() function.
	 * 			| while( ( (!this.isOutsideWorld()) || (world.isAdjacent(x, this.getY(), radius)) ) && ( (this.isOutsideWorld()) || (!world.isAdjacent(x, this.getY(), radius))  )) {
	 * 			|	this.setY(this.getY()- 0.1*radius);
	 * @effect	Changes the position of the worm's projectiles according to the worm's position.
	 * 			| projectile.setPosition(this.calculateProjectilePosition(projectile));
	 * @effect	Decreases the hitpoint's of the worm with the falldamage incured by this fall using the setHitPoints() method.
	 * 			| this.setHitPoints(this.getHitPoints() - fallDamageInt);
	 * @post	If the worm is outside the world, it is removed from the world.
	 * 			| if(this.isOutsideWorld()) {
	 * 			| 	this.getWorld().removeWorm(this);
	 * @effect	If the worm can eat, it will eat using the eat() method.
	 * 			| this.eat()
	 * @throws	IllegalArgumentException
	 * 			| catch (IllegalArgumentException)
	 * */
	public void fall() throws IllegalArgumentException {
		
		if(!this.getWorld().isImpassable(this.getX(), this.getY(), this.getRadius())) {
			double x = this.getX();
			double prevY = this.getY();
			double radius = this.getRadius();
			World world = this.getWorld();
			while( ( (!this.isOutsideWorld()) || (world.isAdjacent(x, this.getY(), radius)) ) && ( (this.isOutsideWorld()) || (!world.isAdjacent(x, this.getY(), radius))  )) {
				this.setY(this.getY()- this.getWorld().getStepSize());
				for (Projectile projectile : this.getWormProjectiles()) {
					projectile.setPosition(this.calculateProjectilePosition(projectile));
				}
			}
			
			long fallDamage = Math.round(Math.floor( 3 * (prevY - this.getY()) ));
			int fallDamageInt = 0;
			if(fallDamage>=Integer.MAX_VALUE) {
				fallDamageInt = this.getHitPoints();
			} else {
				fallDamageInt = (int) fallDamage;
			}
			this.setHitPoints(this.getHitPoints() - fallDamageInt);
		
			if(this.isOutsideWorld()) {
				this.getWorld().removeWorm(this);
			} 
			
			try {
				this.eat();
			} catch (IllegalArgumentException exc) {
				throw new IllegalArgumentException(exc);
			}
		}
	};
	
	/**
	 * Returns whether or not the given worm can fall down
	 * @return	Returns whether or not the given worm can fall down
	 * 			| return !this.getWorld().isAdjacent(this.getX(), this.getY(), this.getRadius());
	 */
	public boolean canFall() {
		return !this.getWorld().isAdjacent(this.getX(), this.getY(), this.getRadius());
	};

	/**
	 * Returns whether or not the given worm can move.
	 * @return	Returns whether or not the given worm can move.
	 * 			| result == this.getMoveCost() <= this.getActionPoints() && this.isActive();
	 */
	public boolean canMove() {
		this.calculateMove();
		return this.getMoveCost() <= this.getActionPoints() && this.isActive();
		
	};
	
	/**
	 * Calculates the move, moves if possible, decreases the actionpoints by the movecost of this move, let's the worm eat if possible and changes the position of the worm's projectile according to the worm's position.
	 * @effect	Calculates the move using the calculateMove() method.
	 * 			| this.calculateMove()
	 * @effect	Sets the x and y coordinate to the new X and Y coordinate using the setX() and setY() method.
	 * 			| 	double newX = this.getX() + (this.getMoveDistance() * Math.cos(this.getMoveDirection()));
	 * 			|	double newY = this.getY() + (this.getMoveDistance() * Math.sin(this.getMoveDirection()));
	 * 			|		if( Util.fuzzyEquals(this.getMoveDistance(), 0.0) ) {
	 * 			|			newX = this.getX() + this.getRadius() * Math.cos(this.getDirection()) * 2;
	 * 			|			newY = this.getY() + this.getRadius() * Math.sin(this.getDirection()) * 2;
	 * 			|				if(!this.getWorld().isImpassable(newX, newY, this.getRadius() )) {
	 * 			|					this.setX( newX );
	 * 			|					this.setY( newY );
	 * 			|				} else {
	 * 			|					this.setX( newX );
	 * 			|					this.setY( newY );
	 * 			|				}
	 * 			|		}
	 * @effect	Decreases the action points with the moveCost using the setActionPoints() method.
	 * 			| this.setActionPoints(this.getActionPoints() - this.getMoveCost());
	 * @effect	Let's the worm eat if it can eat.
	 * 			| this.eat()
	 * @effect	Changes the position of the worm's projectiles according to the worm's position.
	 * 			| projectile.setPosition(this.calculateProjectilePosition(projectile)); 
	 * @throws	IllegalArgmentException
	 * 			| catch (IllegalArgumentException)
	 */
	public void move() throws IllegalArgumentException {
		this.calculateMove();
		if((! this.canMove()) )
			throw new IllegalArgumentException();
		else {	
			double newX = this.getX() + (this.getMoveDistance() * Math.cos(this.getMoveDirection()));
			double newY = this.getY() + (this.getMoveDistance() * Math.sin(this.getMoveDirection()));
			
			if( Util.fuzzyEquals(this.getMoveDistance(), 0.0) ) {
				double moveDistanceI = this.walksIntoPassable();
				newX = this.getX() + moveDistanceI * Math.cos(this.getDirection());
				newY = this.getY() + moveDistanceI * Math.sin(this.getDirection());
				
				if(!this.getWorld().isImpassable(newX, newY, this.getRadius())) {
					this.setX( newX );
					this.setY( newY );
					
					long longMoveCost = Math.round( Math.abs( (Math.cos(this.getDirection())  + Math.abs( 4*Math.sin( this.getDirection() ) )) * moveDistanceI ) );
					if (longMoveCost >= Integer.MAX_VALUE) {
						this.setMoveCost(this.getMaximumActionPoints());
					} else {
						this.setMoveCost((int) longMoveCost); 
					}	
				}
			} else {
				this.setX( newX );
				this.setY( newY );
			}
			
			this.setActionPoints(this.getActionPoints() - this.getMoveCost());
			this.setMoveDirection(0);
			this.setMoveDistance(0);
			this.setMoveCost(0);
		}
		try {
			this.eat();
		} catch (IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
		
		for (Projectile projectile : this.getWormProjectiles()) {
			projectile.setPosition(this.calculateProjectilePosition(projectile));
		}
	};
	
	/**
	 * This method is designed for when the calculateMove() method cannot find any position to move to (no adjacent location found in direction +/- 0.785).
	 * It calculates how far the worm can go in its direction no further or maximum a whole radius.
	 * @ return		Returns a double to help the move method calculate how far to move in the current direction.
	 * 				| while((!this.getWorld().isImpassable(x, y, radius)) &&  (i<radius) ) {
	 * 				| 	x = this.getX() + i * Math.cos(direction);
	 * 				| 	y = this.getY() + i * Math.sin(direction);
	 * 				| 	i = i + this.getWorld().getStepSize();
	 * 				| }
	 * 				| return i;
	 */
	private double walksIntoPassable() {
		double x = this.getX();
		double y = this.getY();
		double radius = this.getRadius();
		double direction = this.getDirection();
		double i = 0.0;
		
		while((!this.getWorld().isImpassable(x, y, radius)) &&  (i<radius) ) {
			x = this.getX() + i * Math.cos(direction);
			y = this.getY() + i * Math.sin(direction);
			i = i + this.getWorld().getStepSize();
		}
		return i; 
	}
	
	/**
	 * Calculates the direction, distance and the move cost for a worm at a certain point in the map and facing a certain direction.
	 * The direction of the move lies between the direction the worm is facing +/- 0.7875 and maximizes the moved distance while minimizing the divergence.
	 * @effect	Sets the moveDirection to the calculated moveDirection using the setMoveDirection() method.
	 * 			| this.setMoveDirection(result[0]);
	 * @effect	Sets the moveDistance to the calculated moveDistance using the setMoveDistance() method.
	 * 			| this.setMoveDistance(result[1]);
	 * @effect	Sets the moveCost to the calculated moveCost using the setMoveCost() method.
	 * 			| this.setMoveCost((int) longMoveCost); 
	 * @throws	IllegalArgumentException
	 * 			| catch (IllegalArgumentException)
	 */
	private void calculateMove() throws IllegalArgumentException {
		double orientation = this.getDirection();
		World world = this.getWorld();
		double x = this.getX();
		double y = this.getY();
		double radius = this.getRadius();
		double maxDistance = 0;
		double maxDistanceDirection = 0;
		double minDivergence = Double.POSITIVE_INFINITY;
		double i = orientation - 0.7875;
		while (Util.fuzzyLessThanOrEqualTo(i, orientation+0.7875)) {
			//double newSteps = 1.5 * this.getWorld().getStepSize();
			double newSteps = 0.1 * this.getRadius();
			double newX = x + Math.cos(i) * newSteps;
			double newY = y + Math.sin(i) * newSteps;
			double newDistance = Math.sqrt( Math.pow( (x - newX), 2) + Math.pow( (y - newY), 2) );
			while( Util.fuzzyLessThanOrEqualTo(newDistance, radius) ) {
				if(Util.fuzzyLessThanOrEqualTo(maxDistance, newDistance) && (world.isAdjacent(newX, newY, radius))) {
					double s = Math.atan( (y - newY) / (x - newX) );
					double newDivergence = Math.abs( i - s );
					if(Util.fuzzyGreaterThanOrEqualTo(minDivergence, newDivergence)) {
						maxDistance = newDistance;
						minDivergence = newDivergence;
						maxDistanceDirection = i;
					}
				
				};
				newSteps =+ this.getWorld().getStepSize();
				newX = newX + Math.cos(i) * newSteps;
				newY = newY + Math.sin(i) * newSteps;
				newDistance = Math.sqrt( Math.pow( (x - newX), 2) + Math.pow( (y - newY), 2) );
			}
			i = i + 0.0175;
		}
		
		double moveCost = Math.abs( (Math.cos(maxDistanceDirection) ) + Math.abs( 4*Math.sin( maxDistanceDirection ) ) * maxDistance);
		double[] result = {maxDistanceDirection, maxDistance, moveCost};
		
		try {
			this.setMoveDirection(result[0]);
			this.setMoveDistance(result[1]);
			long longMoveCost = Math.round( result[2] );
			if (longMoveCost >= Integer.MAX_VALUE) {
				this.setMoveCost(this.getMaximumActionPoints());
			} else {
				this.setMoveCost((int) longMoveCost); 
			}	
		} catch (IllegalArgumentException exc) {
			throw new IllegalArgumentException (exc);
		}
	}
	
	/**
	 * Returns the MoveDirection.
	 */
	@Basic
	private double getMoveDirection() {
		return moveDirection;
	}

	/**
	 * Sets the moveDirection to the given MoveDirection
	 * @param 	moveDirection
	 * 			The moveDirection to set.
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(moveDirection)
	 */
	private void setMoveDirection(double moveDirection) throws IllegalArgumentException {
		if (Double.isNaN(moveDirection))
			throw new IllegalArgumentException();
		this.moveDirection = moveDirection;
	}
	
	private double moveDirection;
	
	/**
	 * Returns the MoveDistance.
	 */
	@Basic
	private double getMoveDistance() {
		return moveDistance;
	}
	
	/**
	 * Sets the moveDistance to the given MoveDistance.
	 * @param 	moveDistance
	 * 			The moveDistance to set.
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(moveDistance)
	 */
	private void setMoveDistance(double moveDistance) throws IllegalArgumentException{
		if (Double.isNaN(moveDistance))
			throw new IllegalArgumentException();
		this.moveDistance = moveDistance;
	}

	private double moveDistance;
	
	/**
	 * Returns the MoveCost.
	 */
	@Basic
	private int getMoveCost() {
		return moveCost;
	}

	/**
	 * Sets the moveCost to the given moveCost
	 * @param 	moveCost
	 * 			The moveCost to set.
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(moveCost)
	 */
	private void setMoveCost(int moveCost) {
		this.moveCost = moveCost;
	}

	private int moveCost;

	/**
	 * Checks if this worm can eat any of the food rations in its world. If so the worm's radius is increased by 10% and the food ration is removed.
	 * @effect	If the worm can eat any of the food rations it grows using the setSizeAspect() method.
	 * 			| if( this.canEat( worldFood.get(i) ) )
	 * 			|		this.setSizeAspects( this.getRadius() * 1.1 )
	 * @effect	This is repositioned using the positionAfterEat() method.
	 * 			| this.positionAfterEat();
	 *@effect	The food ration that is eaten is removed from the worldFood ArrayList using the removeFood() method.
	 *			| this.getWorld().removeFood( worldFood.get(i) );
	 *@throws	IllegalArgumentException
	 *			| catch (IllegalArgumentException)
	 */
	public void eat() throws IllegalArgumentException {
		ArrayList<Food> worldFood = (ArrayList<Food>) this.getWorld().getFood();
		for (int i = 0; i < worldFood.size(); i++) {
			if( this.canEat( worldFood.get(i) ) ) {
				try {
					this.setSizeAspects( this.getRadius() * 1.1 );
				} catch (IllegalArgumentException exc) {
					throw new IllegalArgumentException (exc);
				}
				this.positionAfterEat();
				this.getWorld().removeFood( worldFood.get(i) );
			}
		}
	}
	/**
	 * Changes the position of this worm after eating to the new adjacent location.
	 * @effect	Sets the x and y coordinate of this worm to the new adjacent location using the setX() and setY() method.
	 * 			| while(i<2*Math.PI && !this.getWorld().isAdjacent(newX, newY, this.getRadius()*1.1)) {
	 * 			|	newX = this.getX() + Math.cos(i) * this.getRadius() * 0.1;
	 * 			|	newY = this.getY() + Math.sin(i) * this.getRadius() * 0.1;
	 * 			|	i=i+0.0175;
	 * 			| }
	 * 			| this.setX(newX);
	 * 			| this.setY(newY);
	 */
	public void positionAfterEat() {
		double i = 0.0;
		double newX = this.getX();
		double newY = this.getY();
		while(i<2*Math.PI && !this.getWorld().isAdjacent(newX, newY, this.getRadius())) {
			newX = this.getX() + Math.cos(i) * this.getRadius() * 0.1;
			newY = this.getY() + Math.sin(i) * this.getRadius() * 0.1;
			i=i+0.0175;
		}
		this.setX(newX);
		this.setY(newY);
	}
	
	/**
	 * Returns if this worm can eat the given Food ration.
	 * @param 	food
	 * 			the food ration to check.
	 * @return	If the distance between the worm and the food ration is smaller than the sum of their radiuses, then they overlap and the worm can eat.
	 * 			| result == distance < (food.getRadius() + this.getRadius());
	 */
	public boolean canEat(Food food) {
		double distance = Math.sqrt( Math.pow((food.getX() - this.getX()), 2) + Math.pow((food.getY() - this.getY()), 2) );
		return distance < (food.getRadius() + this.getRadius());
	}
	
	/**
	 * Returns whether or not the given worm can turn by the given angle.
	 * @pre		The angle over which the worm is turned lies between -PI and PI.
	 * 			| -Math.PI <= angle && angle < Math.PI
	 * @param	angle
	 * 			the angle over which to turn.
	 * @return	Returns whether or not the given worm can turn by the given angle.
	 * 			| result == this.calculateTurnCost(angle) <= this.getActionPoints() && this.isActive();
	 */
	public boolean canTurn(double angle) {
		return Util.fuzzyLessThanOrEqualTo( this.calculateTurnCost(angle), this.getActionPoints() ) && this.isActive();
	};
	
	/**
	 * Checks if the given angle is a valid angle over which this worm can turn.
	 * @param 	angle
	 * 			the angle to check.
	 * @return	Returns whether the angle over which the worm is turned is a number and not infinite.
	 * 			| result == !Double.isNaN(angle) && !Double.isInfinite(angle)
	 */
	public boolean isValidTurnAngle(double angle) {
		return !Double.isNaN(angle) && !Double.isInfinite(angle);
	}

	/**
	 * Turns the given worm by the given angle.
	 * @pre		The angle must be valid.
	 * 			| this.isValidTurnAngle(angle)
	 * @pre		This worm must be able to turn.
	 * 			| this.canTurn(angle);
	 * @param	angle
	 * 			The angle over which to turn.
	 * @effect	Sets the direction to the new direction using the setDirection() method.
	 * 			| this.setDirection( restrictDirection( this.getDirection() + angle ) )	
	 * @effect	Changes the position and the direction of the worm's projectiles according to the worm's direction.
	 *  		| projectile.setDirection(this.getDirection());
	 * 			| projectile.setPosition(this.calculateProjectilePosition(projectile)); 
	 * @effect	Decreases the actionPoints with the actionPoints needed for this turn.
	 * 			| this.setActionPoints(this.getActionPoints() - this.calculateTurnCost(angle));
	 */
	public void turn(double angle) {
		assert this.isValidTurnAngle(angle);
		assert this.canTurn(angle);
		this.setDirection( restrictDirection( this.getDirection() + angle ) );
		for (Projectile projectile : this.getWormProjectiles()) {
			projectile.setDirection(this.getDirection());
			projectile.setPosition(this.calculateProjectilePosition(projectile));
		}
		this.setActionPoints(this.getActionPoints() - this.calculateTurnCost(angle));
	};
	
	/**
	 * Calculates the number of action points needed for turning this worm over the given angle.
	 * @param 	angle
	 * 			The angle over which to turn.
	 * @return	Return the number of action points needed for turning this worm over the given angle.
	 * 			|  result == (int) Math.ceil( 60 * Math.abs(angle) / (2*Math.PI) )
	 */
	public int calculateTurnCost(double angle) {
		return (int) Math.ceil( 60 * Math.abs(angle) / (2*Math.PI) );
	}
	
	
	/**
	 * Calculates the jump, makes the worm jump, sets the actionpoints of this worm to 0, let's the worm eat if possible, removes the worm if it is outside the world and updates the position of the worm's projectiles according to the worm's position.
	 * @param	timeStep
	 * 			The elementary time interval in which we may assume that the worm will not completely move through a piece of impassable terrain.
	 * @effect	The method first calculates the force with which the worm will be fired (using the calculateForce() method) and then uses the jump(timeStep, force) method from jumpable to jump.
	 * 			| double force = this.calculateForce();
	 * 			| super.jump(timeStep, force);
	 * @effect	The actionPoints are set to 0 using the setActionPoints() method.
	 * 			|	this.setActionPoints(0);
	 * @effect	If this worm can eat, it will eat! using the eat() method.
	 * 			|this.eat()
	 * @effect	If this worm is outside the world after jumping, it will be removed from the world using the removeWorm() method.
	 * 			| if(this.isOutsideWorld()) {
	 * 			|	this.getWorld().removeWorm(this);
	 * @effect	Changes the position of the worm's projectiles according to the worm's position.
	 * 			| projectile.setPosition(this.calculateProjectilePosition(projectile)); 
	 * @throws 	IllegalArgumentException
	 * 			| Double.isNaN(timeStep) || catch (IllegalArgumentException)
	 * @throws 	UnsupportedOperationException
	 * 			If jump(timeStep, force) from Jumpable throws an UnsupportedOperationException.
	 */
	@Override
	public void jump(double timeStep) throws UnsupportedOperationException, IllegalArgumentException {
		if(this.getActionPoints() == 0 )
			throw new UnsupportedOperationException();
		if (Double.isNaN(timeStep))
			throw new IllegalArgumentException();
		
		try { 
			this.getJumpTime(timeStep);
		} catch (IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
		try {
			double force = this.calculateForce();
			super.jump(timeStep, force);
			this.eat();
			if(this.isOutsideWorld()) {
				this.getWorld().removeWorm(this);
			}
			this.setActionPoints(0);
		} catch(UnsupportedOperationException uOExc) {
			throw uOExc;
		} catch (IllegalArgumentException exc) {
				throw new IllegalArgumentException(exc);
		}
		
		for (Projectile projectile : this.getWormProjectiles()) {
			projectile.setPosition(this.calculateProjectilePosition(projectile));
		}
	};
	
	/**
	 * 
	 * Returns the total amount of time (in seconds) that the jump will take. It returns the previous jumpTime if the position, action points and direction of this worm hasn't changed since this method was last invoked.
	 * @effect	Sets the previous direction to the current direction using the setPrevDirection() method.
	 * 			| this.setPrevDirection(this.getDirection());
	 * @effect	Sets the previous position to the current position using the setPrevPosition() method.
	 *			| this.setPrevPosition(this.getPosition());
	 * @effect	Sets the previous action points to the current action points using the setActionPoints() method.
	 *			| this.setPrevActionPoints(this.getActionPoints());
	 * @effect	Sets the previous jump time to the current direction using the setPrevJumpTime() method.
	 * 			| this.setPrevJumpTime(jumpTime);
	 * @return	Returns the previous jumpTime if the position and direction of this worm hasn't changed since this method was last invoked.
	 * 			| if(  (this.getDirection() == prevDirection) && (this.getPosition().isEqual(prevPosition) && (this.getActionPoints() == this.getPrevActionPoints())) ) then result == prevJumpTime
	 * @return	Returns the total amount of time (in seconds) that the jump will take.
	 *			The method increases the jumpTime with the given timeStep and checks if the position on that moment is not adjacent and passible.
	 *			If both of these are true, it will remember this jumTime and continue the function.
	 *			Next the method checks if the position on that moment is either adjacent or outside the world.
	 *			If any of this two conditions is correct, it will return the jumpTime, only if the distance of the jump is bigger than the worms radius.
	 *			If this is not the case (jump distance is smaller than worm's radius), the method will return 0.0.
	 *			| while(this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius()) && !this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius()) )  {
	 *			|	jumpTime = jumpTime + timeStep;
	 *			|	this.setPrevJumpTime(jumpTime);
	 *			|	distance = Math.sqrt( Math.pow( (this.getX() - this.getJumpStep(jumpTime).getX()), 2) + Math.pow( (this.getY() - this.getJumpStep(jumpTime).getY()), 2) );
	 *			|	jumpStep = getJumpStep(jumpTime);
	 *			| } 
	 *			| if(!this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius())) {
	 *			| 	while(!( 
	 *			| 			(this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius())
	 *			| 			|| (this.getWorld().isOutOfWorld(jumpStep))) ) 
	 *			| 		) {
	 *			| 		jumpTime = jumpTime + timeStep;
	 *			| 		this.setPrevJumpTime(jumpTime);
	 *			| 		distance = Math.sqrt( Math.pow( (this.getX() - this.getJumpStep(jumpTime).getX()), 2) + Math.pow( (this.getY() - this.getJumpStep(jumpTime).getY()), 2) );
	 *			| 		jumpStep = getJumpStep(jumpTime);
	 *			| 	};
	 *			| }
	 *			| 
	 *			| if(distance>this.getRadius()) {
	 *			| 	return prevJumpTime;
	 *			| } else {
	 *			| 	this.setPrevJumpTime(0.0);
	 *			| 	return prevJumpTime;
	 *			| }
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(timeStep)
	 * @throws	IllegalArgumentException
	 * 			| distance < this.getRadius()
	 *  
	 */
	@Override
	public double getJumpTime(double timeStep) throws IllegalArgumentException {
		if(Double.isNaN(timeStep)) 
				throw new IllegalArgumentException();
		
		if(this.getActionPoints()!=0 && this.getIsSelected() && this.isActive()) {
		
			double distance = 0.0;
			if( !( (this.getDirection() == prevDirection) && (this.getPosition().isEqual(prevPosition) && (this.getActionPoints() == this.getPrevActionPoints())) ) ) {
				
				double jumpTime = timeStep;
				distance = Math.sqrt( Math.pow( (this.getX() - this.getJumpStep(jumpTime).getX()), 2) + Math.pow( (this.getY() - this.getJumpStep(jumpTime).getY()), 2) );
				Position jumpStep = getJumpStep(jumpTime);
				
				while(this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius()) && !this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius()) )  {
					jumpTime = jumpTime + timeStep;
					this.setPrevJumpTime(jumpTime);
					distance = Math.sqrt( Math.pow( (this.getX() - this.getJumpStep(jumpTime).getX()), 2) + Math.pow( (this.getY() - this.getJumpStep(jumpTime).getY()), 2) );
					jumpStep = getJumpStep(jumpTime);
				} 
				
				if(!this.getWorld().isImpassable(jumpStep.getX(), jumpStep.getY(), this.getRadius())) {
					while(!( 
							(this.getWorld().isAdjacent(jumpStep.getX(), jumpStep.getY(), this.getRadius())
							|| (this.getWorld().isOutOfWorld(jumpStep))) ) 
						) {
						jumpTime = jumpTime + timeStep;
						this.setPrevJumpTime(jumpTime);
						distance = Math.sqrt( Math.pow( (this.getX() - this.getJumpStep(jumpTime).getX()), 2) + Math.pow( (this.getY() - this.getJumpStep(jumpTime).getY()), 2) );
						jumpStep = getJumpStep(jumpTime);
					};
				}
				
				this.setPrevDirection(this.getDirection());
				this.setPrevPosition(this.getPosition());
				this.setPrevActionPoints(this.getActionPoints());
				this.setPrevJumpDistance(distance);
				
				if(distance>this.getRadius()) {
					return prevJumpTime;
				} else {
					this.setPrevJumpDistance(0.0);
					throw new IllegalArgumentException();
				}
				
			} else {
				if (this.getPrevJumpDistance() == 0.0) {
					throw  new IllegalArgumentException();
				}
				return this.getPrevJumpTime();
			}
	
		} else {
			return 0.0; 
		}
		
	};
	
	/**
	 * Returns the previous jump distance of this worm.
	 */
	private double getPrevJumpDistance() {
		return this.prevJumpDistance;
	}
	
	/**
	 * Sets the previous jump distance to the given jump distance.
	 * @param 	jump distance
	 * 			Jump distance to be set.
	 * @post	Sets the previous jump distance to the given jump distance.
	 * 			| new.getJumpDistance() == jump distance
	 */
	private void setPrevJumpDistance(double jumpDistance) {
		this.prevJumpDistance = jumpDistance;
	}
	
	private double prevJumpDistance;
	
	/**
	 * Returns the previous action points of this worm.
	 */
	private int getPrevActionPoints() {
		return this.prevActionPoints;
	}
	
	/**
	 * Sets the previous action points to the given action points.
	 * @param 	actionPoints
	 * 			Action points to be set.
	 * @post	Sets the previous action points to the given action points.
	 * 			| new.getActionPoints() == actionPoints
	 */
	private void setPrevActionPoints(int actionPoints) {
		this.prevActionPoints = actionPoints;
	}
	
	private int prevActionPoints;
	
	/**
	 * Calculates the force with which this worm will jump.
	 * @return	Returns the force with which this worm will jump.
	 * 			| result == (5 * this.getActionPoints()) + this.getMass() * gravity
	 */
	@Override
	public double calculateForce() {
		return (5 * this.getActionPoints()) + this.getMass() * gravity;
	}

	
	/**
	 * Sets the team of this worm to the given team.
	 * @param 	team	
	 * 			The team of this worm.
	 * @post 	The team of this worm is the given team.
	 * 			| new.getTeam() == team
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * Returns the name of the team of the given worm, or returns null if this worm is not part of a team.
	 * @return	Returns the name of the team of the given worm, or returns null if this worm is not part of a team.
	 * 			| if(this.getTeam() == null) { result == null;} else {result == this.getTeam().getName();}
	 */
	public String getTeamName() {
		if(this.getTeam() == null) {
			return null;
		} else {
			return this.getTeam().getName();
		}	
	};
	
	/**
	 * Returns the team object of the given the given worm, or null if this worm is not part of a team.
	 * @return result == this.team
	 */
	@Basic
	public Team getTeam() {
		return this.team;
	}
	
	private Team team;
	
	
	/**
	 * Activates the next weapon for the given worm
	 * @effect	Deselects the current weapon. Selects the next weapon or the first one if the current one is the last one in the ArrayList.
	 * 			| if (indexThisWeapon == this.getWormProjectiles().size()-1 ) {
	 * 			| 		this.getWormProjectiles().get(indexThisWeapon).deactivate();
	 * 			| 		this.getWormProjectiles().get(0).activate();
	 * 			| } else {
	 * 			| 		this.getWormProjectiles().get(indexThisWeapon).deactivate();
	 * 			| 		this.getWormProjectiles().get(indexThisWeapon + 1).activate();
	 * 			| }
	 */
	public void selectNextWeapon() {
		int indexThisWeapon = this.getIndexSelectedWeapon();
		if ( (indexThisWeapon < 0) || (indexThisWeapon >= this.getWormProjectiles().size()) ) {
		};
		if (indexThisWeapon == this.getWormProjectiles().size()-1 ) {
			this.getWormProjectiles().get(indexThisWeapon).deactivate();
			this.getWormProjectiles().get(0).activate();
		} else {
			this.getWormProjectiles().get(indexThisWeapon).deactivate();
			this.getWormProjectiles().get(indexThisWeapon + 1).activate();
		}
	};


	/**
	 * Returns the name of the weapon that is currently active for this worm, or null if no weapon is active.
	 * @return 	Returns the name of the weapon that is currently active for this worm, or null if no weapon is active.
	 * 			| if(projectile.isActive()) 
	 *			|		return projectile.getName();
	 */
	public String getNameSelectedWeapon() {
		for (Projectile projectile : this.getWormProjectiles())
			if(projectile.isActive()) 
				return projectile.getName();
		return null;
	};
	
	/**
	 * Returns the selected weapon.
	 * @return	Returns the selected weapon.
	 * 			| if(projectile.isActive()) 
	 * 			|	return projectile;
	 */
	public Projectile getSelectedWeapon() {
		for (Projectile projectile : this.getWormProjectiles())
			if(projectile.isActive()) 
				return projectile;
		return null;
	}
	
	/**
	 * Returns the index of the selected weapon.
	 * @return	Returns the index of the selected weapon.
	 * 			| if(this.getWormProjectiles().get(i).isActive()) {
	 * 			|		return i;
	 */
	private int getIndexSelectedWeapon() {
		for (int i = 0; i < this.getWormProjectiles().size(); i++) {
			if(this.getWormProjectiles().get(i).isActive()) {
				return i;
			} 
		}
		return -1;
	}
	
	/**
	 * Adds the given projectile to the wormProjectiles ArrayList.
	 * @param 	projectile
	 * 			The projectile to add.
	 * @post	Adds the given projectile to the wormProjectiles ArrayList.
	 * 			| wormProjectiles.add(projectile);
	 */
	public void addProjectile(Projectile projectile) {
		wormProjectiles.add(projectile);
	}
	
	/**
	 * Returns all the Projectiles of this worm as a ArrayList.
	 * @return	Returns all the Projectiles of this worm as a ArrayList.
	 * 			| result == this.wormProjectiles
	 */
	@Basic
	public ArrayList<Projectile> getWormProjectiles() {
		return this.wormProjectiles;
	}
	
	public ArrayList<Projectile> wormProjectiles = new ArrayList<Projectile>();
	
	/**
	 * Calculates the position of the given projectile when owned by the worm.
	 * @param 	projectile
	 * 			The projectile for which we calculate the position.
	 * @return	Returns the position of the given projectile when owned by the worm.
	 * 			| double x = this.getX() + Math.cos( this.getDirection() ) * (this.getRadius() + projectile.getRadius() ) ;
	 * 			| double y = this.getY() + Math.sin( this.getDirection() ) * (this.getRadius() + projectile.getRadius() );
	 * 			| result == new Position(x, y);
	 */
	public Position calculateProjectilePosition(Projectile projectile) {
		double x = this.getX() + Math.cos( this.getDirection() ) * (this.getRadius() + projectile.getRadius() ) ;
		double y = this.getY() + Math.sin( this.getDirection() ) * (this.getRadius() + projectile.getRadius() );
		Position result = new Position(x, y);
		return result;
	}
	
	/**
	 * Returns whether the worm can shoot.
	 * @return	Returns whether the worm can shoot.
	 * 			| result == this.getActionPoints() >= this.getSelectedWeapon().getActionPointsNeeded() && this.isActive();
	 */
	public boolean canShoot() {
		return ( this.getActionPoints() >= this.getSelectedWeapon().getActionPointsNeeded() && this.isActive() );
	}
	
	/**
	 * Makes the given worm shoot its active weapon with the given propulsion yield.
	 * @param	yield
	 * 			The yield with which we shoot the projectile.
	 * @effect	The local projectile variable selectedWeapon is set to a clone of the selected weapon of this worm using the clone() method.
	 * 			| Projectile selectedWeapon = this.getSelectedWeapon().clone();
	 * @effect	The yield of selectedWeapon is set to the given yield using the setYield() method.
	 * 			| selectedWeapon.setYield(yield);
	 * @effect	The actionPoints of this worm are decreased by the number of action points needed to fire this projectile using the setActionPoints() method.
	 * 			| this.setActionPoints( this.getActionPoints() - selectedWeapon.getActionPointsNeeded() );
	 * @effect	The active projectile of the world of this worm is set to selectedweapon.
	 * 			| this.getWorld().setActiveProjectile( selectedWeapon );
	 * @throws	UnsupportedOperationException
	 * 			| ! this.getActionPoints() >= selectedWeapon.getActionPointsNeeded()
	 */
	public void shoot(int yield) throws UnsupportedOperationException {		
		Projectile selectedWeapon = this.getSelectedWeapon().clone(); 
		
		if(canShoot()) {
			selectedWeapon.setYield(yield);
			this.setActionPoints( this.getActionPoints() - selectedWeapon.getActionPointsNeeded() );
			this.getWorld().setActiveProjectile( selectedWeapon );
		} else {
			throw new UnsupportedOperationException();
		}
	};

}
