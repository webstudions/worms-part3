package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
/**
 * @invar	The radius of this game object is a number at all times bigger than 0 and smaller than positive infinity.
 * 			| isValidRadius(this.getRadius());
 * @author 	nickscheynen
 * @author 	carmenvandeloo
 */
public abstract class GameObject {
	
	/**
	 * Initializes this GameObject with the given position (x and y) and radius.
	 * @param 	world
	 * 			The world in which to place the created GameObject
	 * @param 	x
	 * 			The x coordinate for this GameObject in meters.
	 * @param 	y
	 * 			The y coordinate for this GameObject in meters.
	 * @param 	radius
	 * 			The radius of this GameObject in meters
	 * @post	This GameObject is active on initialization.
	 * 			| new.isActive() == true
	 * @effect	The position of this game object is set using the given x and y coordinates.
	 * 			| this.setPosition( new Position(x,y) );
	 * @post	The world that this GameObject is placed in is the given World.
	 * 			| new.getWorld() == world
	 * @effect	The radius of this worm is equal to the given radius. The mass and number of maximum action points are set accordingly.
	 * 			| this.setRadius(radius)
	 * @throws	IllegalArgumentException
	 * 			| !canGetAsWorld(world)
	 */
	@Raw
	protected GameObject(World world, double x, double y, double radius) {
		this.active = true;
		this.world = world;
		this.setPosition( new Position(x,y) );
		this.setRadius(radius);
		if(!this.canGetAsWorld(world))
			throw new IllegalArgumentException();
	}
	
	/**
	 * Sets the x coordinate of this GameObject to the given value.
	 * @post	Sets the x coordinate of this GameObject to the given value.
	 * 			| new.getX() == x
	 * @param 	x	
	 * 			The x coordinate of this GameObject in meters. 
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(x)
	 */
	@Raw
	public void setX(double x) throws IllegalArgumentException {
		try {
			this.position = new Position(x, this.getY());
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException(exc);
		}
	}
	
	/**
	 * Returns the x-coordinate of the given game object.
	 */
	@Basic
	public double getX() {
		return this.position.getX();
	};
	
	/**
	 * Sets the y coordinate of this GameObject to the given value.
	 * @post	Sets the y coordinate of this GameObject to the given value.
	 * 			| new.getY() == y
	 * @param 	y
	 * 			The y coordinate of this GameObject in meters.
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(x)
	 */
	@Raw
	public void setY(double y) throws IllegalArgumentException {
		try {
			this.position = new Position(this.getX(), y);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Returns the y-coordinate of the given game object.
	 */
	@Basic
	public double getY() {
		return this.position.getY();
	};
	
	/**
	 * Sets the position of this GameObject to the given position
	 * @param 	position	
	 * 			The position of this GameObject
	 * @post	Sets the position of this GameObject to the given position
	 * 			| new.getPosition() == position 
	 * @throws	IllegalArgumentException
	 * 			| position == null
	 */
	@Raw
	public void setPosition(Position newPosition) {
		if(newPosition == null) 
			throw new IllegalArgumentException();
		this.position = newPosition;
	}
	
	/**
	 * Returns the position object of the given game object.
	 */
	@Basic
	public Position getPosition() {
		return this.position.clone();
	}
	
	private Position position;
	
	/**
	 * Return the radius of this GameObject, in meters.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Sets the radius of this GameObject to the given radius.
	 * @post	Sets the radius of this GameObject to the given radius.
	 * 			| new.getRadius() == radius
	 * @param 	radius
	 * 			The radius of this GameObject in meters.
	 * @throws 	IllegalArgumentException
	 * 			| ! this.isValidRadius(radius)
	 */
	@Raw
	protected void setRadius(double radius) throws IllegalArgumentException {
		if(!this.isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
	}
	
	/**
	 * Checks if the given radius is valid for this GameObject
	 * @param 	radius
	 * 			| The radius of this GameObject in meters
	 * @return	Returns true if the given radius is valid for this GameObject
	 * 			| result == !Double.isNaN(radius) && radius < double.POSITIVE_INFINITY && radius > 0.0
	 */
	public boolean isValidRadius(double radius) {
		return !Double.isNaN(radius) && radius < Double.POSITIVE_INFINITY && radius > 0.0;
	}

	protected double radius;
	
	/**
	 * @param world
	 * @return	Returns if this GameObject can get this world as world
	 * 			| result == this.isActive() && (world != null) && (world.getGameState() == GameState.INITIALISING)
	 */
	private boolean canGetAsWorld(World world) {
		return (this.isActive() && (world != null) && (world.getGameState() == GameState.INITIALISING));
	}
	
	/**
	 * Return the world of this GameObject.
	 */
	@Basic @Immutable
	public World getWorld() {
		return this.world;
	}
	
	private final World world;
	
	/**
	 * Returns whether this game object is outside the world or not.
	 * @return	Returns whether this game object is outside the world or not.
	 * 			| (this.getX() > this.getWorld().getWidth()) || (this.getX() < 0) || (this.getY() > this.getWorld().getHeight()) || (this.getY() < 0));
	 */
	public boolean isOutsideWorld() {
		return ( (this.getX() + this.getRadius() > this.getWorld().getWidth()) || 
				 (this.getX() - this.getRadius() < 0) || 
				 (this.getY() + this.getRadius()> this.getWorld().getHeight()) ||
				 (this.getY() - this.getRadius() < 0));
	}
	
	/**
	 * Returns whether or not the given food ration is alive (active).
	 * @return	Returns whether or not the given food ration is alive (active)
	 * 			| this.active
	 */
	public boolean isActive() {
		return this.active;
	};
	
	/**
	 * Sets the active status of this game object to false (DIE!).
	 */ 
	public void DIE() {
		this.active = false;
	}
	
	protected boolean active;
}
