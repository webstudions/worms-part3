package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.*;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Team {
	/**
	 * Initializes this team with the given world and name.
	 * @param 	world
	 * 			The world in which this team is active.
	 * @param 	name
	 * 			The name of this team.
	 * @effect	The world of this team is set using the setWorld() method.
	 * 			| this.setWorld(world);
	 * @effect	The name of this team is set using the setName() method.
	 * 			| this.setName(name);
	 */
	@Raw
	public Team(World world, String name) {
		this.setWorld(world);
		this.setName(name);
	}
	
	private World world;
	
	/**
	 * Returns the world object of this team.
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Sets the world of this team to the given world.
	 * @param 	world
	 * 			The world in which this world active.
	 * @post	The world of this team is the given world.
	 * 			| new.getWorld == world
	 */
	public void setWorld(World world) {
		this.world = world;
	}
	
	/**
	 * Returns the name of this team.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this team to the given name.
	 * @param 	name
	 * 			The name of this team.
	 * @post	The name of this team is the given name.
	 * 			| new.getName() == name
	 * @throws	IllegalArgumentException
	 * 			| !isValidName(name)
	 */
	public void setName(String name) throws IllegalArgumentException {
		if(!isValidName(name))
			throw new IllegalArgumentException();
		this.name = name;
	}
	
	/**
	 * Check if the name is valid.
	 * @param 	name
	 * 			The name of this team.
	 * @return	Return true if the name matches the regular expression. 
	 * 			Names must start with a capital letter, be at least 2 characters long and 
	 * 			can only use letters (upper case and lower case).
	 * 			| result == name.matches("[A-Z]{1}[a-zA-Z]+");
	 */
	public boolean isValidName(String name) {
		return name.matches("[A-Z]{1}[a-zA-Z]+");
	}

	private String name;
	
	/**
	 * Adds the given worm to this team.
	 * @post	The given worm is added to the teamWorms arraylist.
	 * 			| new.getWorms().contains(worm)
	 */
	public void addNewWorm(Worm worm) {
		this.getWorms().add(worm);
	};
	
	/**
	 * Returns all the worms in this team as an ArrayList.
	 */
	public ArrayList<Worm> getWorms() {
		return this.teamWorms;
	};
	
	private ArrayList<Worm> teamWorms = new ArrayList<Worm>();
	
}
