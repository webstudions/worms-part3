package worms.model;

import be.kuleuven.cs.som.annotate.Raw;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Rifle extends Projectile {
	/**
	 * Initializes this Rifle with the given world, position (x and y), radius and direction.
	 * @param 	world
	 * 			The world in which to place the created Rifle object
	 * @param 	x
	 * 			The x coordinate for this Rifle object in meters.
	 * @param 	y
	 * 			The y coordinate for this Rifle object in meters.
	 * @param 	direction
	 * 			The direction this Rifle object is facing in radians.
	 * @effect	This Rifle is initialized as a Jumpable object with world as world, x as x, y as y and direction as direction.
	 * 			| super(world, x, y, Projectile.calculateRadius(rifleMass), direction);
	 * @effect	The mass of this Rifle object is set using the setMass() method.
	 * 			| new.getMass() == rifleMass
	 */
	@Raw
	public Rifle(World world, double x, double y, double direction) {
		super(world, x, y, Projectile.calculateRadius(rifleMass),direction);
		this.setMass();
	}

	/**
	 * Calculates the force with which this rifle will be shot, depending on the yield.
	 * @return	Returns the force with which this rifle will be shot, depending on the yield.
	 * 			| result == (this.getYield()/100) * 7 + 2.5;
	 */
	@Override
	public double calculateForce() {
		return 1.5;
	}
	
	/**
	 * Sets the mass of this rifle to the mass of all rifles.
	 * @post	Sets the mass of this rifle to the mass of all rifles.
	 * 			| new.getMass() == rifleMass
	 */
	@Override @Raw
	public void setMass() {
		this.mass = rifleMass;
	}
	
	private static final double rifleMass = 0.01;
	
	/**
	 * Gets the name of this rifle.
	 */
	public String getName() {
		return Rifle.name;
	}
	protected static final String name = "Rifle";
	
	/**
	 * Returns the damage this rifle can inflict.
	 */
	@Override
	public int getDamage() {
		return damage;
	}
	
	private static final int damage = 20;
	
	/**
	 * Returns the action points needed to fire this rifle.
	 */
	@Override
	public int getActionPointsNeeded() {
		return actionPointsNeeded;
	}
	
	private static final int actionPointsNeeded = 10;
	
}
