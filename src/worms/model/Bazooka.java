package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Bazooka extends Projectile {
	/**
	 * Initializes this Bazooka with the given world, position (x and y), radius and direction.
	 * @param 	world
	 * 			The world in which to place the created Bazooka object
	 * @param 	x
	 * 			The x coordinate for this Bazooka object in meters.
	 * @param 	y
	 * 			The y coordinate for this Bazooka object in meters.
	 * @param 	direction
	 * 			The direction this Bazooka object is facing in radians.
	 * @effect	This Bazooka is initialized as a jumpable object with world as world, x as x, y as y and direction as direction.
	 * 			| super(world, x, y, Projectile.calculateRadius(bazookaMass), direction);
	 * @effect	The mass of this Bazooka object is set using the setMass() method.
	 * 			| new.getMass() == bazookaMass
	 */
	@Raw
	public Bazooka(World world, double x, double y, double direction) {
		super(world, x, y, Projectile.calculateRadius(bazookaMass), direction);
		this.setMass();
	}
	
	/**
	 * Calculates the force with which this bazooka will be shot, depending on the yield.
	 * @return	Returns the force with which this bazooka will be shot, depending on the yield.
	 * 			| result == (this.getYield()/100) * 7 + 2.5;
	 */
	@Override
	public double calculateForce() {
		return (this.getYield()/100) * 7 + 2.5;
	}
	
	/**
	 * Sets the mass of this bazooka to the mass of all bazooka's
	 * @post	Sets the mass of this bazooka to the mass of all bazooka's.
	 * 			| new.getMass() == bazookaMass
	 */
	@Override @Raw
	public void setMass() {
		this.mass = bazookaMass;
	}
	
	private static final double bazookaMass = 0.3;
	
	/**
	 * Gets the name of this bazooka.
	 */
	public String getName() {
		return Bazooka.name;
	}
	protected static final String name = "Bazooka";
	
	/**
	 * Returns the damage this bazooka can inflict.
	 */
	@Override
	public int getDamage() {
		return damage;
	}
	
	private static final int damage = 80;
	
	/**
	 * Returns the action points needed to fire this bazooka.
	 */
	@Override
	public int getActionPointsNeeded() {
		return actionPointsNeeded;
	}
	
	private static final int actionPointsNeeded = 50;

}
