package worms.model;

import be.kuleuven.cs.som.annotate.*;
/**
 * @author nickscheynen
 * @author carmenvandeloo
 */
public class Food extends GameObject {
	
	/**
	 * Create a new food ration that is positioned at the given location in the given world.
	 * @param 	world
	 * 			The world in which to place the created food ration
	 * @param 	x
	 * 			The x-coordinate of the position of the new food ration (in meter)
	 * @param 	y
	 * 			The y-coordinate of the position of the new food ration (in meter)
	 * @effect	This food ration is initialized as a game object with world as world, x as x, y as y and Food.getFoodRadius() as radius.
	 * 			| super(world, x, y, Food.getFoodRadius());
	 */
	@Raw
	public Food(World world, double x, double y) {
		super(world, x, y, Food.getFoodRadius());
	}

	/**
	 * Returns the radius of all food rations
	 */
	@Basic @Immutable
	public static double getFoodRadius() {
		return Food.radius;
	};
	
	private final static double radius = 0.2;

}
