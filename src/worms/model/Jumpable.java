package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import worms.util.Util;

/**
 * @invar	The direction of the worm lies at all times between 0 and 2pi.
 * 			| isValidDirection( this.getDirection() )
 * @invar	The method, getJumpTime() always returns a double that is bigger than or equal to 0.
 * @author 	nickscheynen
 * @author 	carmenvandeloo
 */

public abstract class Jumpable extends GameObject {
	/**
	 * Initializes this Jumpable GameObject with the given world, position (x and y), radius and direction.
	 * @param 	world
	 * 			The world in which to place the created Jumpable object
	 * @param 	x
	 * 			The x coordinate for this Jumpable object in meters.
	 * @param 	y
	 * 			The y coordinate for this Jumpable object in meters.
	 * @param 	radius
	 * 			The radius of this Jumpable object in meters
	 * @param 	direction
	 * 			The direction this Jumpable object is facing in radians.
	 * @effect	This jumpable object is initialized as a game object with world as world, x as x, y as y and radius as radius.
	 * 			| super(world, x, y, radius);
	 * @post	The direction of this jumpable object is set to the given direction.
	 * 			| new.getDirection() == direction
	 */
	@Raw
	public Jumpable(World world, double x, double y, double radius, double direction) {
		super(world, x, y, radius);
		this.setDirection(direction);
	}
	
	/**
	 * Return the mass of this jumpable object, in kilograms.
	 */
	@Basic
	public double getMass() {
		return this.mass;
	}
	
	/**
	 * Sets the mass of this jumpable object.
	 */
	@Raw
	protected abstract void setMass();
	
	protected double mass;
	
	/**
	 * Return the direction this jumpable object is facing, in radians.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * Sets the direction of this jumpable object to the given direction.
	 * @pre		The given direction is a number that lies between 0 and 2 PI.
	 * 			| isValidDirection( direction )
	 * @post	Sets the direction of this jumpable object to the given direction.
	 * 			| new.getDirection() == direction
	 * @param 	direction
	 * 			The direction this jumpable object is going to face in radians.
	 */
	@Raw
	public void setDirection(double direction) {
		assert isValidDirection(direction);
		this.direction = direction;
	}
	
	/**
	 * Converts the given angle into an angle between 0 (included) and 2 PI (not included)
	 * @param 	angle
	 * 			The angle we want to restrict in radians.
	 * @return	Return the given angle converted into an angle between 0 (included) and 2 PI (not included)
	 * 			| while(angle < 0) { angle += 2 * Math.PI; } 
	 * 			| while(angle >= 2 * Math.PI) { angle -= 2 * Math.PI; }
	 * 			| result == angle;
	 */
	public static double restrictDirection(double angle) {
		while (Util.fuzzyLessThanOrEqualTo(angle, 0.0)) {
			angle += 2 * Math.PI;
		}
		while (Util.fuzzyGreaterThanOrEqualTo(angle, 2*Math.PI)) {
			angle -= 2 * Math.PI;
		}
		return angle;
	}
	
	/**
	 * Check if the given direction lies between 0 and 2PI.
	 * @param	direction
	 * 			The direction to check in radians.
	 * @return	Return true if the given direction lies between 0 (included) and 2 PI (included).
	 * 			| result == ( (0 <= direction) && (direction <= 2*MATH.PI) && (! Double.isNaN(direction) )
	 */
	public boolean isValidDirection(double direction) {
		return ( Util.fuzzyLessThanOrEqualTo(0, direction) ) && ( Util.fuzzyLessThanOrEqualTo(direction, 2*Math.PI) && (! Double.isNaN(direction)) );
	}
	
	private double direction;
	
	/**
	 * Makes this jumpable object jump.
	 * @param 	timeStep
	 * 			The elementary time interval in which we may assume that the jumpable object will not completely move through a piece of impassable terrain.
	 * @throws	UnsupportedOperationException
	 * @throws	IllegalArgumentException
	 */
	public abstract void jump(double timeStep) throws UnsupportedOperationException, IllegalArgumentException;
	
	/**
	 * Makes the object jump.
	 * @param	timeStep
	 * 			The elementary time interval in which we may assume that the jumpable object will not completely move through a piece of impassable terrain.
	 * @param	force 
	 * 			The force with which this jumpable object will jump.
	 * @post	The x value of the object is increased by the x distance of the jump.
	 * 			| new.getX() == this.getX() + distanceX
	 * @post	The y value of the object is increased by the y distance of the jump.
	 * 			| new.getY() == this.getY() + distanceY
	 */
	public void jump(double timeStep, double force) {
		double v0 = (force/this.getMass()) * 0.5;
		double distanceX;
		double distanceY;
		distanceX = v0 * Math.cos(this.getDirection()) * this.getJumpTime(timeStep);
		distanceY = v0 * Math.sin(this.getDirection()) * this.getJumpTime(timeStep) - 0.5 * gravity * Math.pow(this.getJumpTime(timeStep), 2);
		this.setX( this.getX() + distanceX );
		this.setY( this.getY() + distanceY );	
	}
	
	/**
	 * Returns the total amount of time (in seconds) that a jump of the given jumpable object would take.
	 * @param	timeStep
	 * 			The elementary time interval in which we may assume that the jumpable object will not completely move through a piece of impassable terrain.
	 * @return	Returns the total amount of time (in seconds) that a jump of the given jumpable object would take.
	 * @throws	IllegalArgumentException
	 */
	public abstract double getJumpTime(double timeStep) throws IllegalArgumentException;
	
	/**
	 * Returns the previous direction of this jumpable object.
	 */
	@Basic
	protected double getPrevDirection() {
		return prevDirection;
	}
	
	/**
	 * Set the previous direction of this jumpable object to the given value.
	 * @param 	prevDirection
	 * 			The direction this jumpable object was facing before.
	 * @post	The previous direction of this jumpable object is equal to the given value.
	 * 			| new.getPrevDirection == prevDirection
	 */
	protected void setPrevDirection(double prevDirection) {
		this.prevDirection = prevDirection;
	}

	protected double prevDirection;
	
	/**
	 * Returns the previous position object of this jumpable object.
	 */
	@Basic
	protected Position getPrevPosition() {
		return prevPosition;
	}
	
	/**
	 * Set the previous position of this jumpable object to the given position.
	 * @param 	prevPosition
	 * 			The previous position of this jumpable object.
	 * @post	The previous position of this jumpable object is equal to the given position.
	 * 			| new.getPosition() == prevPosition
	 */
	protected void setPrevPosition(Position prevPosition) {
		this.prevPosition = prevPosition;
	}
	
	protected Position prevPosition;
	
	/**
	 * Returns the previous jumptime of this jumpable object.
	 */
	@Basic
	protected double getPrevJumpTime() {
		return prevJumpTime;
	}
	
	/**
	 * Set the previous jumptime of this jumpable object to the given value.
	 * @param 	prevJumpTime
	 * 			The jumptime of this jumpable object for the previous direction and previous position.
	 * @post	The previous jumptime of this jumpable object is equal to the given value.
	 * 			| new.getPrevJumpTime == prevJumpTime
	 */
	protected void setPrevJumpTime(double prevJumpTime) {
		this.prevJumpTime = prevJumpTime;
	}

	protected double prevJumpTime;

	/**
	 * Returns the location on the jump trajectory of the given object after a time t.
	 * @param 	t
	 * 			The time for which we get the location on the jump trajectory.
	 * @return 	An array with two elements, with the first element being the x-coordinate and the second element the y-coordinate.
	 * 			| double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
	 * 			| double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
	 * 			| result == {x,y}
	 * @throws	IllegalArgumentException
	 *  		| Double.isNaN(t) || t < 0
	 */
	public Position getJumpStep(double t) throws IllegalArgumentException {
		if(Double.isNaN(t) || t<0) 
			throw new IllegalArgumentException();
		double force = this.calculateForce();
		double v0 = (force/this.getMass()) * 0.5;
		double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
		double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
		Position result = new Position(x, y);
		return result;
	};
	
	/**
	 * Calculates the force with which the jumpable object will jump.
	 * @return	Returns the force with which the jumpable object will jump.
	 */
	public abstract double calculateForce();
	
	protected final double gravity = 9.80665;

}
